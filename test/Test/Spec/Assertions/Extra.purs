module Test.Spec.Assertions.Extra where

import Prelude

import Control.Monad.Error.Class (class MonadError, class MonadThrow, try)
import Data.Either (Either(..))
import Data.Eq (class Eq)
import Data.Foldable (class Foldable, sequence_)
import Data.Maybe (Maybe(..))
import Data.Result (Result(..))
import Data.Result as Result
import Data.Show (class Show)
import Data.Traversable (sequence)
import Data.Unit (Unit)
import Effect.Aff as Aff
import Test.Spec (Spec)
import Test.Spec.Assertions (fail, shouldEqual, shouldSatisfy)

shouldSucceedWith
  :: forall e a m
  .  MonadThrow Aff.Error m
  => Show e
  => Show a
  => Eq e
  => Eq a
  => Result e a
  -> a
  -> m Unit
shouldSucceedWith lhs rhs = lhs `shouldEqual` Ok rhs

-- shouldSucceedWith
--   :: forall r e a
--   .  MonadError e r
--   => Foldable r
--   => Show a
--   => Show e
--   => Eq a
--   => r a
--   -> a
--   -> Spec Unit
-- shouldSucceedWith lhs rhs =
--   let
--     x :: r (Spec Unit)
--     x = try lhs >>= \val ->
--       case val of
--         Right y ->
--           pure $ y `shouldEqual` rhs

--         Left err ->
--           pure $ fail $ "failed with error: " <> show err
--   in
--     sequence_ x

shouldSucceed
  :: forall e a m
  .  MonadThrow Aff.Error m
  => Show e
  => Show a
  => Result e a
  -> m Unit
shouldSucceed val = val `shouldSatisfy` Result.isOk

shouldFailWith
  :: forall e a m
  .  MonadThrow Aff.Error m
  => Show e
  => Show a
  => Eq e
  => Eq a
  => Result e a
  -> e
  -> m Unit
shouldFailWith lhs rhs = lhs `shouldEqual` Error rhs

shouldFail
  :: forall e a m
  .  MonadThrow Aff.Error m
  => Show e
  => Show a
  => Eq e
  => Eq a
  => Result e a
  -> m Unit
shouldFail val = val `shouldSatisfy` Result.isError

shouldBeJust
  :: forall a m
  .  MonadThrow Aff.Error m
  => Show a
  => Eq a
  => Maybe a
  -> a
  -> m Unit
shouldBeJust lhs rhs = lhs `shouldEqual` Just rhs
