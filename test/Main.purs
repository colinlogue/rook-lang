module Test.Main where

import Prelude

import Effect (Effect)
import Effect.Aff (launchAff_)
import Rook.Exec.Test as Exec
import Rook.Parse.Test as Parse
import Rook.Types.Test as Types
import Test.Rook.Compilation.Test as Compilation
import Test.Spec (focus)
import Test.Spec.Reporter (consoleReporter)
import Test.Spec.Runner (runSpec)

main :: Effect Unit
main =
  launchAff_ $ runSpec [consoleReporter]
    do
      Compilation.spec
      Parse.spec
      -- Types.spec
      -- Exec.spec
