module Test.Rook.Test.Parsable where

import Prelude

import Rook.Parse (SourcePos)
import Rook.Parse as Parse
import Rook.Parse.Token (RookParser)
import Rook.Syntax.Common (RookType)
import Rook.Syntax.Literal (Literal)
import Rook.Syntax.Pattern (Pattern)
import Rook.Syntax.Surface (Expr, Stmt)

-- class Parsable a where
--   parse :: RookParser a

-- instance Parsable (Expr SourcePos) where
--   parse = Parse.expr

-- instance Parsable (Pattern SourcePos) where
--   parse = Parse.pattern

-- instance Parsable Literal where
--   parse = Parse.lit

-- instance Parsable (Stmt SourcePos) where
--   parse = Parse.stmt

-- instance Parsable (RookType SourcePos) where
--   parse = Parse.rookType

-- instance Parsable (Expr Unit) where
--   parse = map (const unit) <$> Parse.expr

-- instance Parsable (Pattern Unit) where
--   parse = map (const unit) <$> Parse.pattern

-- instance Parsable (Stmt Unit) where
--   parse = map (const unit) <$> Parse.stmt

-- instance Parsable (RookType Unit) where
--   parse = map (const unit) <$> Parse.rookType
