module Rook.Test.Helpers where

import Prelude
import Rook.Syntax.Common
import Rook.Syntax.Surface

import Data.CodePoint.Unicode as CodePoint
import Data.List (List(..), (:))
import Data.List as List
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Result (Result(..))
import Data.String.CodePoints as String
import Data.Tuple (Tuple(..))
import Debug as Debug
import Effect.Exception (name)
import Partial.Unsafe (unsafeCrashWith)
import Prelude (identity, map, ($), (<<<))
import Rook.Syntax.Literal (Literal(..))
import Rook.Syntax.Pattern (Pattern(..))
import Rook.Types.Unification (TypeConstraint(..))

class ToExpr a where
  toExpr :: a -> Expr Unit

instance toExprExpr :: ToExpr (Expr a) where
  toExpr = map (const unit)

instance toExprString :: ToExpr String where
  toExpr = Ident unit Nil

instance toExprInt :: ToExpr Int where
  toExpr = LitExpr unit <<< IntLit

instance toExprBool :: ToExpr Boolean where
  toExpr = LitExpr unit <<< BoolLit

class ToPattern a where
  toPattern :: a -> Pattern Unit

instance toPatternPattern :: ToPattern (Pattern a) where
  toPattern = map (const unit) >>> identity

instance toPatternString :: ToPattern String where
  toPattern "_" = WildcardPattern unit
  toPattern s = PatternVar unit s

instance toPatternLiteral :: ToPattern Literal where
  toPattern = LiteralPattern unit

class ToStmt a where
  toStmt :: a -> Stmt Unit

instance ToStmt (Stmt a) where
  toStmt = map (const unit)

instance ToStmt (Expr a) where
  toStmt = map (const unit) >>> ExprStmt

class ToType a where
  toType :: a -> RookType Unit

instance ToType (RookType a) where
  toType = map (const unit)

instance ToType String where
  toType s =
    case String.uncons s of
      Just {head} ->
        if CodePoint.isLower head
        then TypeVar unit s
        else TypeAlias unit (s : Nil)

      Nothing ->
        unsafeCrashWith $ "not a valid type: " <> s

instance ToType (Result b (RookType a)) where
  toType r =
    case r of
      Ok t -> void t
      Error _ -> unsafeCrashWith $ "can't convert error to type"

-- class ToValue a where
--   toValue :: a -> Value

-- instance ToValue Value where
--   toValue = identity

-- instance ToValue Int where
--   toValue = LitValue <<< toLiteral

-- instance ToValue Boolean where
--   toValue = LitValue <<< toLiteral

-- instance ToValue Unit where
--   toValue = LitValue <<< toLiteral

class ToLiteral a where
  toLiteral :: a -> Literal

instance ToLiteral Literal where
  toLiteral = identity

instance ToLiteral Int where
  toLiteral = IntLit

instance ToLiteral Boolean where
  toLiteral = BoolLit

instance ToLiteral Unit where
  toLiteral = const UnitLit

instance ToLiteral String where
  toLiteral = StringLit

fun :: forall a b. ToPattern a => ToExpr b => Array a -> b -> Expr Unit
fun ps body = FunExpr unit params Nothing (map (const unit) (toExpr body))
  where
    params :: List (FunParam Unit)
    params =
      List.fromFoldable
        $ map (\x -> { pattern : x, annotation : Nothing })
        $ map toPattern ps

ident :: String -> Expr Unit
ident = Ident unit Nil

call :: forall a b. ToExpr a => ToExpr b => a -> Array b -> Expr Unit
call f args =
  Call (toExpr f) unit
    $ List.fromFoldable
    $ map (toExpr >>> ExprArg) args

call' :: forall a b. ToExpr a => ToExpr b => a -> Array (Maybe b) -> Expr Unit
call' f args =
  Call (toExpr f) unit
    $ List.fromFoldable
    $ map (fromMaybe $ PlaceholderArg unit)
    $ map (map (toExpr >>> ExprArg)) args

ctorPattern :: forall a. ToPattern a => String -> Array a -> Pattern Unit
ctorPattern c args = CtorPattern unit (c : Nil) $ List.fromFoldable $ map toPattern args

block :: forall e s. ToExpr e => ToStmt s => Array s -> e -> Expr Unit
block stmts final = BlockExpr unit (List.fromFoldable $ map toStmt stmts) $ Just $ toExpr final

letStmt :: forall e a. ToExpr e => String -> Maybe (RookType a) -> e -> Stmt Unit
letStmt x t def = DeclStmt unit Let x (map (map $ const unit) t) (toExpr def)

mutStmt :: forall e a. ToExpr e => String -> Maybe (RookType a) -> e -> Stmt Unit
mutStmt x t e = DeclStmt unit Mut x (map (map $ const unit) t) (toExpr e)

assign :: forall e. ToExpr e => String -> e -> Stmt Unit
assign x e = Assign unit Nil x (toExpr e)

forStmt :: forall e p s. ToExpr e => ToPattern p => ToStmt s => p -> e -> Array s -> Stmt Unit
forStmt p e block = For unit (toPattern p) (toExpr e) $ List.fromFoldable $ map toStmt block

arrow :: forall t1 t2. ToType t1 => ToType t2 => Array t1 -> t2 -> RookType Unit
arrow paramTypes returnType = Arrow unit (List.fromFoldable $ map toType paramTypes) (toType returnType)

typeVar :: String -> RookType Unit
typeVar = TypeVar unit

typeAlias :: String -> RookType Unit
typeAlias s = TypeAlias unit (s : Nil)

typeApp :: forall t1 t2. ToType t1 => ToType t2 => t1 -> Array t2 -> RookType Unit
typeApp t args = TypeApp unit (toType t) (List.fromFoldable $ map toType args)

-- funval :: forall e. ToExpr e => Array String -> e -> Value
-- funval params e = FunValue (List.fromFoldable params) Nil (toExpr e)

while :: forall e s. ToExpr e => ToStmt s => e -> Array s -> Stmt Unit
while e stmts = While unit (toExpr e) $ List.fromFoldable $ map toStmt stmts

break :: Stmt Unit
break = Break unit

continue :: Stmt Unit
continue = Continue unit

intType :: RookType Unit
intType = Primitive unit IntType

-- constraint :: forall t1 t2. ToType t1 => ToType t2 => t1 -> t2 -> TypeConstraint
-- constraint t1 t2 = TypeConstraint (toType t1) (toType t2)

forAll :: forall t. ToType t => Array String -> t -> RookType Unit
forAll xs t = Forall unit (List.fromFoldable xs) $ toType t

litExpr :: forall l. ToLiteral l => l -> Expr Unit
litExpr = toLiteral >>> LitExpr unit

ifThen :: forall e1 e2. ToExpr e1 => ToExpr e2 => e1 -> e2 -> Expr Unit
ifThen e1 e2 = If unit (toExpr e1) (toExpr e2) Nothing

ifThenElse :: forall e1 e2 e3. ToExpr e1 => ToExpr e2 => ToExpr e3 => e1 -> e2 -> e3 -> Expr Unit
ifThenElse e1 e2 e3 = If unit (toExpr e1) (toExpr e2) (Just $ toExpr e3)

noStmts :: Array (Stmt Unit)
noStmts = []

match :: forall e1 e2 p. ToExpr e1 => ToPattern p => ToExpr e2 => e1 -> Array ({ pattern :: p, definition :: e2}) -> Expr Unit
match e arms =
  Match unit (toExpr e)
    $ List.fromFoldable
    $ map (\x -> { pattern : toPattern x.pattern, definition : toExpr x.definition }) arms

arm :: forall p e. ToPattern p => ToExpr e => p -> e -> MatchArm Unit
arm p e = { pattern : toPattern p, definition : toExpr e }

loop :: forall s. ToStmt s => Array s -> Stmt Unit
loop stmts = Loop unit $ List.fromFoldable $ map toStmt stmts

exprStmt :: forall e. ToExpr e => e -> Stmt Unit
exprStmt e = ExprStmt $ toExpr e

qualifiedIdent :: Array String -> String -> Expr Unit
qualifiedIdent path name = Ident unit (List.fromFoldable path) name

propAccess :: forall e. ToExpr e => e -> String -> Expr Unit
propAccess e = PropertyAccess (toExpr e) unit

def :: forall t e. ToType t => ToExpr e =>
  Array String -> String -> Array (DefParam _) -> t -> e -> Declaration Unit
def typeVars name params t e =
  Def unit
    (List.fromFoldable typeVars)
    name
    (Just $ List.fromFoldable params)
    (toType t)
    (toExpr e)

block' :: forall s. ToStmt s => Array s -> Expr Unit
block' stmts = BlockExpr unit (List.fromFoldable $ toStmt <$> stmts) Nothing

emptyBlock :: Expr Unit
emptyBlock = BlockExpr unit Nil Nothing

param :: forall t. ToType t => String -> t -> DefParam Unit
param x t = { name : x, annotation : toType t, info : unit }