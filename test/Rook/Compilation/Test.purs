-- | Tests of the whole compilation process.
module Test.Rook.Compilation.Test where

import Prelude

import Data.Bifunctor (lmap)
import Data.Generic.Rep (class Generic)
import Data.List (List(..))
import Data.Maybe (Maybe(..))
import Data.Result (Result(..))
import Data.Result as Result
import Data.Show.Generic (genericShow)
import Data.Traversable (for_)
import Data.Tuple as Tuple
import Rook.Binding (captureExprVars, captureStmtVars)
import Rook.Parse (SourcePos)
import Rook.Parse as Parse
import Rook.Parse.Token (RookParser)
import Rook.Syntax.Bound as Bound
import Rook.Syntax.Surface as Surface
import Rook.Test.Helpers (arm, assign, block, break, call, call', continue, ctorPattern, exprStmt, forStmt, fun, ident, ifThen, ifThenElse, letStmt, litExpr, loop, match, mutStmt, noStmts, propAccess, qualifiedIdent, while)
import Test.Rook.Syntax.Bound.Helpers as Bound
import Test.Spec (Spec, describe, it)
import Test.Spec.Assertions.Extra (shouldSucceed)
import Text.Parsing.Parser (ParseError, runParser)
import Text.Parsing.Parser.String as Parser

data TestError (a :: Type -> Type) (b :: Type -> Type)
  = ParseFail ParseError
  | ParseMismatch (a Unit) (a SourcePos)
  | BoundMismatch (b Unit) (b SourcePos)

-- instance Eq (a Unit) => Eq (TestError a)

derive instance Generic (TestError a b) _

instance (Show (a Unit), Show (a SourcePos), Show (b Unit), Show (b SourcePos)) => Show (TestError a b) where
  show x = genericShow x

type TestResult a b c = Result (TestError a b) c

class Testable a b | a -> b where
  parser :: RookParser (a SourcePos)
  binder :: forall pos. List String -> a pos -> b pos

instance Testable Surface.Expr Bound.Expr where
  parser = Parse.expr
  binder = captureExprVars

instance Testable Surface.Stmt Bound.Stmt where
  parser = Parse.stmt
  binder xs = captureStmtVars xs >>> Tuple.snd

runTest :: forall a b. Eq (a Unit) => Eq (b Unit) => Functor a => Functor b => Testable a b => TestCase a b -> TestResult a b Unit
runTest test =
  do
    surfaceResult <- runParser test.input ((parser :: RookParser (a SourcePos)) <* Parser.eof)
      # Result.fromEither
      # lmap ParseFail
    unless (void surfaceResult == test.surface)
      $ Error $ ParseMismatch test.surface surfaceResult
    let boundResult = binder Nil surfaceResult
    unless (void boundResult == test.bound)
      $ Error $ BoundMismatch test.bound boundResult
    pure unit

type TestCase a b =
  { input :: String
  , surface :: a Unit
  , bound :: b Unit
  }

exprTests :: Array (TestCase Surface.Expr Bound.Expr)
exprTests =
  [ { input : "\"str\""
    , surface : litExpr "str"
    , bound : Bound.litExpr "str"
    }
  , { input : "1"
    , surface : litExpr 1
    , bound : Bound.litExpr 1
    }
  , { input : "-1"
    , surface : litExpr (-1)
    , bound : Bound.litExpr (-1)
    }
  , { input : "x"
    , surface : ident "x"
    , bound : Bound.global "x"
    }
  , { input : "(x)"
    , surface : ident "x"
    , bound : Bound.global "x"
    }
  , { input : "x(y)"
    , surface : call "x" [ ident "y" ]
    , bound : Bound.call "x" [ Bound.global "y" ]
    }
  , { input : "x(y(z))"
    , surface : call "x" [ call "y" [ ident "z"]]
    , bound : Bound.call "x" [ Bound.call "y" [ Bound.global "z"]]
    }
  , { input : "x(y)(z)"
    , surface : call (call "x" [ ident "y" ]) [ ident "z" ]
    , bound : Bound.call (Bound.call "x" [ Bound.global "y" ]) [ Bound.global "z" ]
    }
  , { input : "x => x"
    , surface : fun ["x"] "x"
    , bound : Bound.fun ["x"] (Bound.local 0)
    }
  , { input : "(x) => x"
    , surface : fun ["x"] "x"
    , bound : Bound.fun ["x"] (Bound.local 0)
    }
  , { input : "(x, y) => x"
    , surface : fun ["x", "y"] "x"
    , bound : Bound.fun ["x", "y"] (Bound.local 1)
    }
  , { input : "_ => x"
    , surface : fun [ "_" ] (ident "x")
    , bound : Bound.fun [ "_" ] (Bound.global "x")
    }
  , { input : "Foo(x,y) => y"
    , surface : fun [ ctorPattern "Foo" [ "x", "y"] ] "y"
    , bound : Bound.fun [ ctorPattern "Foo" [ "x", "y"] ] (Bound.local 0)
    }
  , { input : "(x => x)(y)"
    , surface : call (fun [ "x" ] "x") [ ident "y" ]
    , bound : Bound.call (Bound.fun [ "x" ] (Bound.local 0)) [ Bound.global "y" ]
    }
  , { input : "if (x) y"
    , surface : ifThen "x" "y"
    , bound : Bound.ifThen "x" "y"
    }
  , { input : "if (x) y else z"
    , surface : ifThenElse "x" "y" "z"
    , bound : Bound.ifThenElse "x" "y" "z"
    }
  , { input : "{x}"
    , surface : block noStmts (ident "x")
    , bound : Bound.block Bound.noStmts (Bound.global "x")
    }
  , { input : "{ let x := 1; x }"
    , surface : block [ letStmt "x" Nothing 1 ] (ident "x")
    , bound : Bound.block [ Bound.letStmt "x" Nothing 1 ] (Bound.local 0)
    }
  , { input : "f(x, %_)"
    , surface : call' (ident "f") [ Just (ident "x"), Nothing ]
    , bound : Bound.call' (Bound.global "f") [ Just (Bound.global "x"), Nothing ]
    }
  , { input : "match x with { x => x , y => x , y => y }"
    , surface : match "x" [ arm "x" "x", arm "y" "x", arm "y" "y" ]
    , bound : Bound.match "x"
        [ Bound.arm "x" (Bound.local 0)
        , Bound.arm "y" (Bound.global "x")
        , Bound.arm "y" (Bound.local 0)
        ]
    }
  , { input : "A.B.c"
    , surface : qualifiedIdent ["A","B"] "c"
    , bound : Bound.qualifiedIdent ["A","B"] "c"
    }
  , { input : "A.B.c.d"
    , surface : propAccess (qualifiedIdent ["A","B"] "c") "d"
    , bound : Bound.propAccess (Bound.qualifiedIdent ["A","B"] "c") "d"
    }
  ]

stmtTests :: Array (TestCase Surface.Stmt Bound.Stmt)
stmtTests =
  [ { input : "let x := 1"
    , surface : letStmt "x" Nothing 1
    , bound : Bound.letStmt "x" Nothing 1
    }
  , { input : "mut x <- 1"
    , surface : mutStmt "x" Nothing 1
    , bound : Bound.mutStmt "x" Nothing 1
    }
  , { input : "x <- 1"
    , surface : assign "x" 1
    , bound : Bound.assign "x" 1
    }
  , { input : "for x of y {}"
    , surface : forStmt "x" "y" noStmts
    , bound : Bound.forStmt "x" "y" Bound.noStmts
    }
  , { input : "while x {}"
    , surface : while "x" noStmts
    , bound : Bound.while "x" Bound.noStmts
    }
  , { input : "loop {}"
    , surface : loop noStmts
    , bound : Bound.loop Bound.noStmts
    }
  , { input : "break"
    , surface : break
    , bound : Bound.break
    }
  , { input : "continue"
    , surface : continue
    , bound : Bound.continue
    }
  , { input : "x"
    , surface : exprStmt "x"
    , bound : Bound.exprStmt "x"
    }
  , { input : "while (true) { break; }"
    , surface : while true [ break ]
    , bound : Bound.while true [ Bound.break ]
    }
  , { input : "for x of xs { x; let x := 0; x; }"
    , surface : forStmt "x" "xs"
        [ exprStmt "x"
        , letStmt "x" Nothing 0
        , exprStmt "x"
        ]
    , bound : Bound.forStmt "x" "xs"
        [ Bound.exprStmt (Bound.local 0)
        , Bound.letStmt "x" Nothing 0
        , Bound.exprStmt (Bound.local 0)
        ]
    }
  ]

spec :: Spec Unit
spec = do

  describe "expressions" $
    for_ exprTests \test -> it test.input $ shouldSucceed $ runTest test

  describe "statements" $
    for_ stmtTests \test -> it test.input $ shouldSucceed $ runTest test


