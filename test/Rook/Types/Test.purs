module Rook.Types.Test where

-- import Prelude
-- import Rook.Syntax.Bound
-- import Rook.Syntax.Common

-- import Control.Monad.Error.Class (class MonadThrow)
-- import Control.Monad.State.Class as State
-- import Data.Either (Either(..))
-- import Data.Foldable (all)
-- import Data.List (List(..), (:))
-- import Data.List as List
-- import Data.Map as Map
-- import Data.Maybe (Maybe(..))
-- import Data.Maybe as Maybe
-- import Data.Result (Result(..))
-- import Data.Set as Set
-- import Data.Tuple (Tuple(..))
-- import Effect.Exception (Error)
-- import Partial.Unsafe (unsafeCrashWith)
-- import Rook.Binding (captureExprVars)
-- import Rook.Context (Context)
-- import Rook.Context as Context
-- import Rook.Parse as Parse
-- import Rook.Print (print, printConstraint, printConstraints, printSubs)
-- import Rook.Types (SourcePos, TypeContext, TypeDef, TypeError(..), checkExpr, runTC, synthExpr)
-- import Rook.Types.Unification (TypeConstraint(..), freeVars, subAll, unify)
-- import Test.QuickCheck ((===))
-- import Test.QuickCheck as QC
-- import Test.Rook.Syntax.Arbitrary as Fuzz
-- import Test.Rook.Syntax.Bound.Helpers (class ToType, arrow, constraint, forAll, intType, typeVar)
-- import Test.Spec (Spec, describe, focus, it)
-- import Test.Spec.Assertions (fail, shouldEqual, shouldReturn, shouldSatisfy)
-- import Test.Spec.Assertions.Extra (shouldBeJust, shouldFailWith, shouldSucceed, shouldSucceedWith)
-- import Test.Spec.Console as Console
-- import Test.Spec.QuickCheck (quickCheck, quickCheck')
-- import Text.Parsing.Parser (runParser)

-- setType :: String -> RookType _ -> Tuple String { type :: RookType Unit, isMutable :: Boolean, position :: SourcePos }
-- setType name t = Tuple name { type : void t, isMutable : false, position : pos 0 0 }

-- parse :: String -> Expr SourcePos
-- parse str =
--   case runParser str Parse.expr of
--     Right expr ->
--       captureExprVars Nil expr

--     Left _ ->
--       unsafeCrashWith $ "failed to parse expression: " <> str

-- emptyCxt :: TypeContext
-- emptyCxt =
--   { context : Context.empty
--   , constraints : Nil
--   , freshCount : 0
--   , typeVars : Set.empty
--   }

-- synth :: Context TypeDef -> String -> Result TypeError (RookType Unit)
-- synth cxt str =
--   case runParser str Parse.expr of
--     Right expr ->
--       runTC
--         { context : cxt
--         , constraints : Nil
--         , freshCount : 0
--         , typeVars : Set.empty
--         } $ synthExpr $ captureExprVars Nil expr

--     Left _ ->
--       unsafeCrashWith $ "failed to parse expression: " <> str

-- check :: Context TypeDef -> String -> RookType Unit -> Result TypeError Unit
-- check cxt str t =
--   case runParser str Parse.expr of
--     Right expr ->
--       runTC
--         { context : cxt
--         , constraints : Nil
--         , freshCount : 0
--         , typeVars : Set.empty
--         } $ checkExpr t $ captureExprVars Nil expr

--     Left _ ->
--       unsafeCrashWith $ "failed to parse expression: " <> str

-- pos :: Int -> Int -> Position
-- pos a b = Position { line : a, column : b }

-- -- shouldUnifyWith :: forall t1 t2. ToType t1 => ToType t2 => t1 -> t2 -> Spec Unit
-- shouldUnifyWith :: forall m t2. MonadThrow Error m ⇒ ToType t2 ⇒ Result TypeError (RookType Unit) → t2 → m Unit
-- shouldUnifyWith t1 t2 =
--   do
--     t1 # shouldSucceed
--     unify (constraint t1 t2 : Nil) `shouldSatisfy` Maybe.isJust

-- spec :: Spec Unit
-- spec =
--   describe "Typing tests" do

--     describe "synthesis" do

--       it "x : t ⊢ x : t" do

--         let mod = Map.singleton "x" { type : TypeVar unit "t", isMutable : false, position : pos 0 0 }
--         let cxt = Context.empty { globals = Map.singleton Nil mod }
--         synth cxt "x" `shouldSucceedWith` TypeVar unit "t"

--       it "f : a -> b, x : a ⊢ f(x) : b" do

--         let mod = (Map.fromFoldable
--           [ setType "f" $ Arrow unit (TypeVar unit "a" : Nil) (TypeVar unit "b")
--           , setType "x" $ TypeVar unit "a"
--           ])
--         let cxt = Context.empty { globals = Map.singleton Nil mod }
--         synth cxt "f(x)" `shouldSucceedWith` TypeVar unit "b"

--       it "f : Int, x : Int ⊢ f x : err (call to non function)" do

--         let mod = (Map.fromFoldable
--           [ setType "f" $ Primitive unit IntType
--           , setType "x" $ Primitive unit IntType
--           ])
--         let cxt = Context.empty { globals = Map.singleton Nil mod }
--         synth cxt "f(x)" `shouldFailWith`
--           CallToNonFunctionType (pos 1 2) (Primitive unit IntType)

--       it "⊢ if (true) 1 else 2 : Int" do

--         synth Context.empty "if (true) 1 else 2" `shouldSucceedWith`
--           Primitive unit IntType

--       it "⊢ { let x := 1; x } : Int" do

--         synth Context.empty "{ let x := 1; x }" `shouldSucceedWith`
--           Primitive unit IntType

--       it "⊢ { let x := 1; let x := 2; x } : err (shadowing)" do

--         synth Context.empty "{ let x := 1; let x := 2; x}" `shouldFailWith`
--           Shadowing (pos 1 15) "x" (pos 1 3)

--       it "f : a -> b -> c, x : a, y : b ⊢ f(x)(y) : c" do

--         let mod = (Map.fromFoldable
--           [ setType "f" $ arrow ["a"] (arrow ["b"] "c")
--           , setType "x" $ typeVar "a"
--           , setType "y" $ typeVar "b"
--           ])
--         let cxt = Context.empty { globals = Map.singleton Nil mod }
--         synth cxt "f(x)(y)" `shouldSucceedWith` typeVar "c"

--       it "⊢ x => x : [t] t -> t" do

--         synth Context.empty "x => x" `shouldSatisfy`
--           case _ of
--             Ok (Forall _ (Cons t1 Nil) (Arrow _ (TypeVar _ t2 : Nil) (TypeVar _ t3))) ->
--               t1 == t2 && t1 == t3

--             _ ->
--               false

--       it "⊢ (a,b) => b : [t1,t2] (t1,t2) -> t2" do

--         synth Context.empty "(a,b) => b" `shouldSatisfy`
--           case _ of
--             Ok (Forall _ (Cons t1 (Cons t2 Nil)) (Arrow _ (TypeVar _ t3 : TypeVar _ t4 : Nil) (TypeVar _ t5))) ->
--               all identity
--                 [ t1 == t3
--                 , t2 == t4
--                 , t2 == t5
--                 ]

--             _ ->
--               false

--       it "⊢ (x => x)(1) : Int" do

--         synth Context.empty "(x => x)(1)" `shouldSucceedWith` intType

--       it "⊢ ((a,b) => b)(1,2) : Int" do

--         synth Context.empty "((a,b) => b)(1,2)" `shouldSucceedWith` intType

--       it "⊢ f => x => f(x) : [t1,t2] (t1 -> t2) -> t1 -> t2" do

--         synth Context.empty "f => x => f(x)"
--           `shouldUnifyWith` forAll ["t1","t2"] (arrow ["t1"] "t2")

--       it "⊢ (f => x => f(x))(a => a) : [t] t -> t" do

--         synth Context.empty "(f => x => f(x))(a => a)"
--           `shouldUnifyWith` forAll ["x"] "x"

--       it "⊢ (f => x => f(x))(a => a)(1) : Int" do

--         synth Context.empty "(a => a)(1)"
--           `shouldSucceedWith` intType

--       it "⊢ (f => x => y => f(x,y))((a,b) => b)(1)(2) : Int" do

--         synth Context.empty "(f => x => y => f(x,y))((a,b) => b)(1)(2)"
--           `shouldSucceedWith` intType

--     -- describe "checking" do

--     --   it "⊢ (f => x => y => f(x,y))((a,b) => b)(1)(2) : Int" do

--     --     check Context.empty "(f => x => y => f(x,y))((a,b) => b)(1)(2)" intType
--     --       # shouldSucceed

--     describe "unification" do

--       it "{a = b} => [ a ↦ b ]" do

--         let s = unify (constraint "a" "b" : Nil)
--         (s >>= Map.lookup "a") `shouldBeJust` typeVar "b"

--       it "{a = b -> c, a = d -> e} => [ b ↦ d, c ↦ e]" do

--         let s = unify (List.fromFoldable
--           [ constraint "a" (arrow ["b"] "c")
--           , constraint "a" (arrow ["d"] "e")
--           ])
--         (s >>= Map.lookup "b") `shouldBeJust` typeVar "d"

--       it "{a = Int, a = b} => [ a ↦ Int, b ↦ Int ]" do

--         let s = unify (List.fromFoldable
--           [ constraint "a" intType
--           , constraint "a" "b"
--           ])
--         (s >>= Map.lookup "a") `shouldBeJust` intType
--         (s >>= Map.lookup "b") `shouldBeJust` intType

--       it "{t = t0 -> t1, t = t2 -> t2, t0 = Int } => [ t ↦ Int -> Int ]" do

--         let s = unify (List.fromFoldable
--           [ constraint "t" $ arrow ["t0"] "t1"
--           , constraint "t" $ arrow ["t2"] "t2"
--           , constraint "t2" intType
--           ])
--         (s >>= Map.lookup "t") `shouldBeJust` arrow [intType] intType


--       -- it "{a = b -> c -> d, b = c -> d, d = Int} => "

--       -- focus $ it "fuzz"
--       --   let
--       --     p :: Fuzz.RookType -> Fuzz.RookType -> QC.Result
--       --     p (Fuzz.RookType t1) (Fuzz.RookType t2) =
--       --       case unify (constraint t1 t2 : Nil) of
--       --         Just s ->
--       --           subAll s t1 === subAll s t2

--       --         Nothing ->
--       --           QC.Failed
--       --             $ "unification failed for " <> print t1 <> ", " <> print t2
--       --   in
--       --     quickCheck' 1 p

--       it "{ [a] a = [b] b } => []" do

--         let s = unify (constraint (forAll ["a"] "a") (forAll ["b"] "b") : Nil)
--         s `shouldSatisfy` Maybe.isJust

--     describe "free vars" do

--       it "fv(a) = { a }" do

--         freeVars (typeVar "a") `shouldEqual` Set.singleton "a"

--       it "fv([a,b] (b,c) -> a) = { c }" do

--         freeVars (forAll ["a","b"] (arrow ["b","c"] "a"))
--           `shouldEqual` Set.singleton "c"