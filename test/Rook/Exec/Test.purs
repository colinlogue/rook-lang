module Rook.Exec.Test where

-- import Prelude
-- import Rook.Test.Helpers

-- import Control.Monad.Comp (runComp)
-- import Data.Either (Either(..))
-- import Data.Identity (Identity(..))
-- import Data.Maybe (Maybe(..))
-- import Data.Result (Result(..))
-- import Data.Tuple (Tuple(..))
-- import Data.Tuple as Tuple
-- import Partial.Unsafe (unsafeCrashWith)
-- import Rook.Context as Context
-- import Rook.Exec (class Interp, Exec, ExecError, ExecContext)
-- import Rook.Exec as Exec
-- import Rook.Parse as Parser
-- import Rook.Syntax (Expr, Value(..))
-- import Test.Spec (Spec, describe, it)
-- import Test.Spec.Assertions (shouldEqual)
-- import Test.Spec.Assertions.Extra (shouldSucceed, shouldSucceedWith)
-- import Text.Parsing.Parser (runParser)

-- newtype TestInterp a = TestInterp (Maybe a)

-- derive newtype instance Functor TestInterp
-- derive newtype instance Apply TestInterp
-- derive newtype instance Applicative TestInterp
-- derive newtype instance Bind TestInterp
-- derive newtype instance Monad TestInterp

-- instance Interp TestInterp where
--   read = pure "test input"
--   write _ = pure unit

-- runTestInterp :: forall a. ExecContext -> Exec TestInterp a -> Result ExecError a
-- runTestInterp cxt ex =
--   case runComp cxt ex of
--     TestInterp (Just result) ->
--       map Tuple.snd result

--     TestInterp Nothing ->
--       unsafeCrashWith "test interp error???"


-- evalTest :: forall a. ToValue a => String -> a -> Spec Unit
-- evalTest str expected =
--   case runParser str Parser.expr of
--     Right expr ->
--       let
--         actual :: Result ExecError Value
--         actual = runTestInterp base (Exec.eval $ void expr)
--       in
--         it str $ actual `shouldEqual` Ok (toValue expected)

--     Left _ ->
--       unsafeCrashWith $ "failed to parse " <> str

-- base :: ExecContext
-- base = Context.empty

-- spec :: Spec Unit
-- spec =
--   describe "Execution tests" do

--     describe "expression evaluation" do

--       evalTest "1" $ 1

--       evalTest "x => x" $ funval ["x"] (local 0)

--       evalTest "{ let x := 1; x }" $ 1

--       evalTest "(x => x)(1)" $ 1

--       evalTest "((x,y) => y)(1,2)" $ 2

--       -- evalTest "{ while true { break; } ; true}" $ true

--       evalTest "(f => x => y => f(x,y))((a,b) => b)(1)(2)" $ 2