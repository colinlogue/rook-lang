module Test.Rook.Syntax.Arbitrary where

-- import Prelude

-- import Control.Lazy (defer)
-- import Control.Monad.State.Class (class MonadState)
-- import Data.Array.NonEmpty as NonEmptyArray
-- import Data.Char.Gen (genAlphaLowercase)
-- import Data.List (List(..))
-- import Data.List as List
-- import Data.Map (Map)
-- import Data.Maybe (Maybe(..))
-- import Data.Maybe as Maybe
-- import Data.Newtype (class Newtype, unwrap, wrap)
-- import Data.String.CodeUnits as String
-- import Data.Tuple (Tuple(..))
-- import Data.Tuple as Tuple
-- import Partial.Unsafe (unsafeCrashWith)
-- import Rook.Syntax as Syntax
-- import Test.QuickCheck (class Arbitrary, arbitrary)
-- import Test.QuickCheck.Gen (Gen, chooseInt, listOf)
-- import Test.QuickCheck.Gen as Gen

-- newtype RookType = RookType (Syntax.RookType Unit)

-- derive instance Newtype RookType _


-- arbitraryTypeVar :: List String -> Gen (Maybe RookType)
-- arbitraryTypeVar vars =
--   case vars of
--     Nil ->
--       pure Nothing

--     Cons x xs ->
--       Gen.elements (NonEmptyArray.cons' x (List.toUnfoldable xs))
--         <#> Syntax.TypeVar unit
--         <#> wrap
--         <#> Just

-- arbitraryTypeApp :: List String -> Int -> Gen RookType
-- arbitraryTypeApp xs k =
--   do
--     t <- unwrap <$> genType
--     n <- chooseInt 1 5
--     ts <- map unwrap <$> listOf n genType
--     pure $ wrap $ Syntax.TypeApp unit t ts
--   where
--     genType = defer \_ -> arbitraryType xs k

-- arbitraryForall :: List String -> Int -> Gen RookType
-- arbitraryForall xs k =
--   do
--     n <- chooseInt 1 5
--     ys <- listOf n genParam
--     t <- unwrap <$> arbitraryType (xs <> ys) k
--     pure $ wrap $ Syntax.Forall unit ys t

-- arbitraryArrow :: List String -> Int -> Gen RookType
-- arbitraryArrow xs k =
--   do
--     n <- chooseInt 0 5
--     argTypes <- listOf n genType
--     returnType <- genType
--     pure $ wrap $ Syntax.Arrow unit argTypes returnType
--   where
--     genType = unwrap <$> defer \_ -> arbitraryType xs k

-- arbitraryType :: List String -> Int -> Gen RookType
-- arbitraryType xs k =
--   if
--     k < 0
--   then
--     arbitraryPrimType
--   else
--     oneOf
--       [ arbitraryTypeApp xs (k - 1)
--       , arbitraryArrow xs (k - 1)
--       , arbitraryForall xs (k - 1)
--       , arbitraryPrimType
--       , do
--           t <- arbitraryTypeVar xs
--           case t of
--             Just x ->
--               pure x

--             Nothing ->
--               genType
--       ]
--   where
--     genType = defer \_ -> arbitraryType xs k

-- genParam :: Gen String
-- genParam = String.singleton <$> genAlphaLowercase

-- arbitraryPrimType :: Gen RookType
-- arbitraryPrimType =
--   elements
--     $ map (RookType <<< Syntax.Primitive unit)
--         [ Syntax.IntType
--         , Syntax.StringType
--         , Syntax.BoolType
--         , Syntax.UnitType
--         , Syntax.NeverType
--         ]

-- oneOf :: forall a. Array (Gen a) -> Gen a
-- oneOf arr =
--   case NonEmptyArray.fromArray arr of
--     Just x -> Gen.oneOf x
--     Nothing -> unsafeCrashWith "oneOf given an empty array"

-- elements :: forall a. Array a -> Gen a
-- elements arr =
--   case NonEmptyArray.fromArray arr of
--     Just x -> Gen.elements x
--     Nothing -> unsafeCrashWith "oneOf given an empty array"

-- instance Arbitrary RookType where
--   arbitrary = arbitraryType Nil 1



-- -- genUnifiableType :: RookType -> Gen RookType
-- -- genUnifiableType = map Tuple.snd <<< go 0
-- --   where
-- --     go :: Int -> RookType -> Gen (Tuple Int RookType)
-- --     go k t =
-- --       do
-- --         n <- chooseInt 0 1
-- --         if
-- --           n == 1
-- --         then
-- --           pure $ Tuple (k + 1) $ wrap $ Syntax.TypeVar unit $ "_" <> show k
-- --         else case unwrap t of
-- --           Syntax.Forall _ xs u ->
-- --             map (wrap <<< Syntax.Forall unit xs <<< unwrap) <$> go k (wrap u)

-- --           Syntax.Arrow _ us u ->
-- --             do
-- --               Tuple k' us' <- foldr (\cur g -> g >>= \(Tuple i acc) -> go i acc cur ) (pure $ Tuple k Nil) us
-- --               Tuple k'' u' <- go k'

-- --           _ ->
-- --             pure $ Tuple k t

