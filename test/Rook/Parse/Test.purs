module Rook.Parse.Test (spec) where

import Prelude
import Rook.Syntax.Common
import Rook.Syntax.Surface
import Rook.Test.Helpers

import Data.List (List(..), (:))
import Data.Maybe (Maybe(..))
import Data.Result (Result)
import Data.Result as Result
import Data.Tuple (Tuple(..))
import Rook.Parse as Parse
import Rook.Parse.Token (RookParser)
import Rook.Syntax.Literal (Literal(..))
import Rook.Syntax.Pattern (Pattern(..))
import Test.Spec (Spec, describe, focus, it)
import Test.Spec.Assertions.Extra (shouldSucceedWith)
import Text.Parsing.Parser (ParseError, runParser)

class Parsable a where
  parse :: RookParser a

instance parsableExpr :: Parsable (Expr Unit) where
  parse = map (const unit) <$> Parse.expr

instance parsablePattern :: Parsable (Pattern Unit) where
  parse = map (const unit) <$> Parse.pattern

instance parsableLiteral :: Parsable Literal where
  parse = Parse.lit

instance parsableStmt :: Parsable (Stmt Unit) where
  parse = map (const unit) <$> Parse.stmt

instance parsableRookType :: Parsable (RookType Unit) where
  parse = map (const unit) <$> Parse.rookType

instance parsableDeclaraion :: Parsable (Declaration Unit) where
  parse = map (const unit) <$> Parse.declaration

parseTest :: forall a. Show a => Eq a => Parsable a => String -> a -> Spec Unit
parseTest str x =
  it str $ (runParser str parse # Result.fromEither) `shouldSucceedWith` x

spec :: Spec Unit
spec =
  describe "Parsing tests" do

    describe "Literals" do

      parseTest "\"str\"" $ StringLit "str"

      parseTest "1" $ IntLit 1

      parseTest "-1" $ IntLit (-1)

    describe "Patterns" do

      parseTest "_" $ WildcardPattern unit

      parseTest "x" $ PatternVar unit "x"

      parseTest "True" $ ctorPattern "True" ([] :: Array String)

      parseTest "Just(x)" $ ctorPattern "Just" [ "x" ]

      parseTest "Foo(x,_)" $ ctorPattern "Foo" [ "x", "_" ]

      parseTest "Foo(Bar)" $ ctorPattern "Foo" [ ctorPattern "Bar" ([] :: Array String) ]

      parseTest "\"str\"" $ LiteralPattern unit $ StringLit "str"

      parseTest "1" $ LiteralPattern unit $ IntLit 1

      parseTest "-1" $ LiteralPattern unit $ IntLit (-1)

      parseTest "Foo(\"str\")" $ ctorPattern "Foo" [ StringLit "str" ]

    describe "Expressions" do

      parseTest "\"str\"" $ litExpr "str"

      parseTest "1" $ litExpr 1

      parseTest "-1" $ litExpr (-1)

      parseTest "x" $ ident "x"

      parseTest "(x)" $ ident "x"

      parseTest "x(y)" $ call "x" [ ident "y" ]

      parseTest "x(y(z))" $ call "x" [ call "y" [ ident "z"]]

      parseTest "x(y)(z)" $ call (call "x" [ ident "y" ]) [ ident "z" ]

      parseTest "x => x" $ fun ["x"] "x"

      parseTest "(x) => x" $ fun ["x"] "x"

      parseTest "(x, y) => x" $ fun ["x", "y"] "x"

      parseTest "_ => x" $ fun [ "_" ] (ident "x")

      parseTest "Foo(x,y) => y" $ fun [ ctorPattern "Foo" [ "x", "y"] ] "y"

      parseTest "(x => x)(y)" $ call (fun [ "x" ] "x") [ ident "y" ]

      parseTest "if (x) y" $ If unit (ident "x") (ident "y") Nothing

      parseTest "if (x) y else z" $ If unit (ident "x") (ident "y") (Just (ident "z"))

      parseTest "{x}" $ block ([] :: Array (Stmt Unit)) (ident "x")

      parseTest "{ let x := 1; x }" $ block [ letStmt "x" Nothing 1 ] (ident "x")

      parseTest "f(x, %_)" $ call' (ident "f") [ Just (ident "x"), Nothing ]

      parseTest "match x with { x => x , y => x , y => y }"
        $ match "x" [ arm "x" "x", arm "y" "x", arm "y" "y" ]

      parseTest "true" $ litExpr true

      parseTest "if (true) 1 else 2"
        $ If unit (litExpr true) (litExpr 1)
        $ Just (litExpr 2)

      -- parseTest "{ while true { break; } true }"
      --   $ block
      --       (While unit (litExpr true) (Break unit : Nil) : Nil)
      --       (litExpr true)

      parseTest "f => x => f(x)"
        $ fun ["f"] $ fun ["x"] $ call (ident "f") [ident "x"]

      parseTest "f => x => y => f(x,y)"
        $ fun ["f"] $ fun ["x"] $ fun ["y"]
        $ call (ident "f") [ident "x", ident "y"]

      parseTest "x => x => x" $ fun ["x"] $ fun ["x"] $ ident "x"

      parseTest "(f => x => y => f(x,y))((a,b) => b)"
        $ call (fun ["f"] $ fun ["x"] $ fun ["y"] $ call (ident "f") [ident "x", ident "y"])
          [fun ["a", "b"] $ ident "b"]

      parseTest "(f => x => y => f(x,y))((a,b) => b)(1)"
        $ call
          (call (fun ["f"] $ fun ["x"] $ fun ["y"] $ call (ident "f") [ident "x", ident "y"])
            [fun ["a", "b"] $ ident "b"])
          [ 1 ]

      parseTest "(f => x => y => f(x,y))((a,b) => b)(1)(2)"
        $ call (call
          (call (fun ["f"] $ fun ["x"] $ fun ["y"] $ call (ident "f") [ident "x", ident "y"])
          [fun ["a", "b"] $ ident "b"])
          [ 1 ]) [ 2 ]

      parseTest "((a,b) => b)(1,2)" $ call (fun ["a","b"] "b") [1,2]


    describe "Statements" do

      parseTest "let x := 1" $ letStmt "x" Nothing 1

      parseTest "mut x <- 1" $ mutStmt "x" Nothing 1

      parseTest "x <- 1" $ assign "x" 1

      parseTest "for x of y {}" $ forStmt "x" "y" ([] :: Array (Stmt Unit))

      parseTest "while x {}" $ While unit (ident "x") Nil

      parseTest "loop {}" $ Loop unit Nil

      parseTest "break" $ Break unit

      parseTest "continue" $ Continue unit

      parseTest "x" $ ExprStmt (ident "x")

      parseTest "while (true) { break; }" $ while true [ break ]

      parseTest "for x of xs { x; let x := 0; x; }"
        $ forStmt "x" "xs"
          [ toStmt (ident "x")
          , letStmt "x" Nothing 0
          , toStmt (ident "x")
          ]

    describe "Types" do

      parseTest "a" $ typeVar "a"

      parseTest "T" $ typeAlias "T"

      parseTest "T<a>" $ typeApp "T" ["a"]

      parseTest "(T)<a>" $ typeApp "T" ["a"]

      parseTest "(a,b) -> c" $ arrow ["a","b"] "c"

      parseTest "a -> b" $ arrow ["a"] "b"

      parseTest "(T<a>)<b -> c, U<d>>"
        $ typeApp (typeApp "T" ["a"]) [arrow ["b"] "c", typeApp "U" ["d"]]

    describe "Declarations" do

      parseTest "def main() : Unit := {}"
        $ def [] "main" [] (typeAlias "Unit") emptyBlock

      parseTest "def [a] identity(x: a) : a := x"
        $ def ["a"] "identity" [ param "x" "a" ] "a" "x"