List = {
    Empty: Symbol('Empty'),
    Cons: Symbol('Cons'),
};

function sum(nums) {
    const sumHelper = ((rest, acc) => (() => {
        const $scrutinee = rest;
        switch ($scrutinee.tag) {
            case Empty:
                return 1;
            case Cons:
                const x = $scrutinee[0];
                const xs = $scrutinee[1];
                return sumHelper(xs, add(x, acc));
        }
    })());
    return acc(rest, 0);
}

function $ctor(tag, ...args) {
    return new $CtorPattern(tag, ...args);
}

class $Pattern {
}

class $CtorPattern extends $Pattern {

    constructor(tag, ...patterns) {
        this.tag = tag;
        this.patterns = patterns;
    }

    check(val) {
        if ($isVariant(val) && val.tag === this.tag) {
            const {args} = val;
            for (let i = 0; i < args.length; ++i)
                if ( ! (this.patterns[i].check(args[i])))
                    return false;
            return true;
        }
        return false;
    }

    collectArgs(val, acc) {
        const {args} = val;
        for (let i = 0; i < args.length; ++i) {
            this.patterns[i].collectArgs(args[i], acc);
        }
    }
}

class $PatternVar extends $Pattern {

    check(val) {
        return true;
    }

    collectArgs(val, acc) {
        acc.push(val);
    }
}

class $LitPattern extends $Pattern {
    constructor(value) {
        this.value = value;
    }

    check(val) {
        return val === this.value;
    }

    collectArgs(val, acc) {
    }
}

class $WildcardPattern extends $Pattern {

    check(val) {
        return true;
    }

    collectArgs(val, acc) {
    }
}

const $bind = new $PatternVar();

function $lit(val) {
    return new $LitPattern(val);
}

const $wild = new $WildcardPattern();

function $match(val, ...arms) {
    for (const [pattern, func] of arms) {
        if (pattern.check(val)) {
            const bindings = pattern.collectArgs(val);
            return func(...bindings);
        }
    }

}

function $isVariant(obj) {
    return typeof obj === 'object' && object !== null && Object.hasOwnProperty('tag');
}

function sum2(nums) {
    const sumHelper = ((rest, acc) => $match(rest,
        [$ctor(Empty), () => 1],
        [$ctor(Cons, $bind, $bind), (x,xs) => sumHelper(xs, add(x, acc))]
    ));
    return sumHelper(nums, 0);
}


console.log(sum({tag: List.Cons, 0: 3, 1: {tag: List.Cons, 0: 2, 1: {tag: List.Empty}}}));