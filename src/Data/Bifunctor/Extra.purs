module Data.Bifunctor.Extra where

import Data.Bifunctor

infixl 4 lmap as <@$>
infixl 1 lmapFlipped as <@#>

lmapFlipped
  :: forall f a1 b
  . Bifunctor f
  => f a1 b
  -> forall a2
  . (a1 -> a2)
  -> f a2 b
lmapFlipped x f = lmap f x

rmapFlipped
  :: forall f a b1
  . Bifunctor f
  => f a b1
  -> forall b2
  . (b1 -> b2)
  -> f a b2
rmapFlipped x f = rmap f x

