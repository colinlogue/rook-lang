module Data.Stack
  ( Stack
  , asList
  , empty
  , push
  , pushMany
  , pop
  , isEmpty
  , peek
  , peekAt
  , depth
  , fromFoldable
  , toUnfoldable
  , locate
  , locateBy
  , findBy
  , StackView
  , fromView
  , up
  , down
  , popMany
  )
  where

import Prelude

import Data.Foldable (class Foldable, foldl)
import Data.List (List(..), (:))
import Data.List as List
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..))
import Data.Tuple as Tuple
import Data.Unfoldable (class Unfoldable, unfoldr)

data Stack a = Stack Int (List a)

derive instance eqStack :: Eq a => Eq (Stack a)
derive instance ordStack :: Ord a => Ord (Stack a)
instance showStack :: Show a => Show (Stack a) where
  show :: Stack a -> String
  show (Stack _ vals) = go vals identity
    where
      go :: List a -> (String -> String) -> String
      go list f =
        case list of
          Nil -> f "Stack.empty"
          Cons x xs -> go xs \s ->
            "(Stack.push (" <> show x <> ") " <> s <> ")"

asList :: forall a. Stack a -> List a
asList (Stack _ list) = list

empty :: forall a. Stack a
empty = Stack 0 Nil

push :: forall a. a -> Stack a -> Stack a
push x (Stack n xs) = Stack (n + 1) (x : xs)

pushMany :: forall f a. Foldable f => f a -> Stack a -> Stack a
pushMany xs stack = foldl (flip push) stack xs

pop :: forall a. Stack a -> Maybe (Tuple (Stack a) a)
pop (Stack n list) =
  case list of
    Nil -> Nothing
    Cons x xs -> Just $ Tuple (Stack (n - 1) xs) x

isEmpty :: forall a. Stack a -> Boolean
isEmpty (Stack n _) = n == 0

peek :: forall a. Stack a -> Maybe a
peek (Stack _ list) =
  case list of
    Nil -> Nothing
    Cons x _ -> Just x

peekAt :: forall a. Int -> Stack a -> Maybe a
peekAt i (Stack n list) =
  if
    i >= n
  then
    List.index list i
  else
    Nothing

depth :: forall a. Stack a -> Int
depth (Stack n _) = n

fromFoldable :: forall f a. Foldable f => f a -> Stack a
fromFoldable = foldl (flip push) empty

toUnfoldable :: forall f a. Unfoldable f => Stack a -> f a
toUnfoldable = unfoldr (pop >>> map Tuple.swap)

locate :: forall a. Eq a => a -> Stack a -> Maybe Int
locate x = locateBy (eq x)

locateBy :: forall a. (a -> Boolean) -> Stack a -> Maybe Int
locateBy pred (Stack _ vals) = go 0 vals
  where
    go :: Int -> List a -> Maybe Int
    go i list =
      case list of
        Nil ->
          Nothing

        Cons cur rest ->
          if
            pred cur
          then
            Just i
          else
            go (i + 1) rest

findBy :: forall a. (a -> Boolean) -> Stack a -> Maybe a
findBy pred (Stack _ vals) = go vals
  where
    go :: List a -> Maybe a
    go = case _ of
      Nil ->
        Nothing

      Cons cur rest ->
        if
          pred cur
        then
          Just cur
        else go rest

type StackView a =
  { earlier :: Stack a
  , later :: Stack a
  , current :: a
  }

fromView :: forall a. StackView a -> Stack a
fromView { earlier , current, later : Stack _ laterVals } =
  pushMany (current : laterVals) earlier

down :: forall a. StackView a -> Maybe (StackView a)
down { earlier, current, later } =
  do
    Tuple rest top <- pop earlier
    Just { earlier : rest, current : top, later : push current later }

up :: forall a. StackView a -> Maybe (StackView a)
up { earlier, current, later } =
  do
    Tuple rest top <- pop later
    Just { earlier : push current earlier, current : top, later : rest }

popMany :: forall a. Int -> Stack a -> Maybe (StackView a)
popMany = \i stack ->
  do
    when (i < 0) Nothing
    Tuple rest top <- pop stack
    go { earlier : rest, current : top, later : empty } (i - 1)
  where
    go :: StackView a -> Int -> Maybe (StackView a)
    go view i =
      if
        i == 0
      then
        Just view
      else do
        view' <- down view
        go view' (i - 1)
