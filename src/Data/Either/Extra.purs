module Data.Either.Extra where

import Data.Either (Either(..))
import Data.Foldable (foldr)
import Data.List (List(..), (:))
import Data.Tuple (Tuple(..))


separate
  :: forall a b
  . List (Either a b)
  -> Tuple (List a) (List b)
separate = foldr sep (Tuple Nil Nil)
  where
    sep :: Either a b -> Tuple (List a) (List b) -> Tuple (List a) (List b)
    sep val (Tuple xs ys) =
      case val of
        Right y ->
          Tuple xs (y : ys)

        Left x ->
          Tuple (x : xs) ys