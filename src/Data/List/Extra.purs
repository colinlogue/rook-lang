module Data.List.Extra where

import Prelude

import Data.Bifunctor (lmap)
import Data.Foldable (foldl)
import Data.List (List(..), (:))
import Data.List as List
import Data.Tuple (Tuple(..))
import Data.Tuple as Tuple
import Debug as Debug

-- TODO: This works but could be made tail recursive.
splitAt :: forall a. Int -> List a -> Tuple (List a) (List a)
splitAt i list =
  if
    i <= 0
  then
    Tuple Nil list
  else
    case list of
      Nil ->
        Tuple Nil Nil

      Cons x xs ->
        map (Cons x) $ splitAt (i - 1) xs