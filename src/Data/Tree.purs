module Data.Tree where

import Prelude

import Data.Foldable (class Foldable)
import Data.List (List(..), foldMap, intercalate, (:))
import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype, unwrap, wrap)
import Data.Traversable (foldl, foldr)

data Tree a = Node a (List (Tree a))

derive instance Functor Tree

children :: forall a. Tree a -> List (Tree a)
children (Node _ subs) = subs

newtype PreOrder a = PreOrder (Tree a)

derive newtype instance Functor PreOrder
derive instance Newtype (PreOrder a) _

preOrder :: forall a. Tree a -> List a
preOrder (Node x subs) = x : foldMap preOrder subs

instance Foldable PreOrder where
  foldl f x = unwrap >>> preOrder >>> foldl f x
  foldr f x = unwrap >>> preOrder >>> foldr f x
  foldMap f = unwrap >>> preOrder >>> foldMap f

newtype PostOrder a = PostOrder (Tree a)

derive newtype instance Functor PostOrder
derive instance Newtype (PostOrder a) _

postOrder :: forall a. Tree a -> List a
postOrder (Node x subs) = foldMap postOrder subs <> pure x

instance Foldable PostOrder where
  foldl f x = unwrap >>> postOrder >>> foldl f x
  foldr f x = unwrap >>> postOrder >>> foldr f x
  foldMap f = unwrap >>> postOrder >>> foldMap f


