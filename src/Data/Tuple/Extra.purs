module Data.Tuple.Extra where

import Data.Tuple (Tuple(..))


setFirst :: forall a b. a -> Tuple a b -> Tuple a b
setFirst x (Tuple _ y) = Tuple x y

setSecond :: forall a b. b -> Tuple a b -> Tuple a b
setSecond y (Tuple x _) = Tuple x y