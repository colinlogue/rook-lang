module Data.Map.Extra where

import Prelude

import Data.Map (Map)
import Data.Map as Map
import Data.Maybe (Maybe(..))


insertUnique :: forall k v. Ord k => k -> v -> Map k v -> Maybe (Map k v)
insertUnique k v m = case Map.lookup k m of
  Just _ -> Nothing
  Nothing -> Just $ Map.insert k v m