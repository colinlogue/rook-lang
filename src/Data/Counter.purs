module Data.Counter where

import Prelude

import Control.Monad.State (State, runState)
import Control.Monad.State as State
import Data.Tuple (Tuple(..))


type Counter = State Int

incr :: Counter Unit
incr = State.modify_ (\i -> i + 1)

decr :: Counter Unit
decr = State.modify_ (\i -> i - 1)

incrBy :: Int -> Counter Unit
incrBy k = State.modify_ (\i -> i + k)

decrBy :: Int -> Counter Unit
decrBy k = State.modify_ (\i -> i - k)

count :: Counter Int
count = State.get

runCounter :: forall a. Counter a -> Tuple a Int
runCounter c = runState c 0

