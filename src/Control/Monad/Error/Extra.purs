module Control.Monad.Error.Extra where

import Prelude

import Control.Monad.Error.Class (class MonadThrow, throwError)
import Data.Maybe (Maybe(..))


note :: forall e a m. MonadThrow e m => e -> Maybe a -> m a
note err maybeX =
  case maybeX of
    Just x ->
      pure x
    
    Nothing ->
      throwError err