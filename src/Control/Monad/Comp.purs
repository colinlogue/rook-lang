module Control.Monad.Comp where

import Prelude

import Control.Monad.Error.Class (class MonadError, class MonadThrow, liftEither, throwError)
import Control.Monad.State.Class (class MonadState)
import Data.Bifunctor (class Bifunctor, bimap)
import Data.Result (Result(..))
import Data.Result as Result
import Data.Tuple (Tuple(..))
import Data.Tuple as Tuple
import Effect (Effect)
import Effect.Aff (Aff)
import Effect.Aff.Class (class MonadAff)
import Effect.Class (class MonadEffect, liftEffect)



type CompResult (m :: Type -> Type) c e a =
  m (Result e (Tuple c a))

newtype Comp m c e a =
  Comp (c -> m (Result e (Tuple c a)))

runComp :: forall m c e a. c -> Comp m c e a -> CompResult m c e a
runComp cxt (Comp f) = f cxt

instance functorComp:: Functor m => Functor (Comp m c e) where
  map :: forall a b. (a -> b) -> Comp m c e a -> Comp m c e b
  map f (Comp g) = Comp $ map (map (map (map f))) g

instance applyComp :: Monad m => Apply (Comp m c e) where
  apply :: forall a b. Comp m c e (a -> b) -> Comp m c e a -> Comp m c e b
  apply (Comp f) (Comp x) =
    Comp \cxt ->
      do
        res <- f cxt
        case res of
          Ok (Tuple cxt' f') ->
            do
              res' <- x cxt'
              case res' of
                Ok (Tuple cxt'' x') ->
                  pure $ Ok $ Tuple cxt'' $ f' x'

                Error e ->
                  pure $ Error e

          Error e ->
            pure $ Error e

instance applicativeComp :: Monad m => Applicative (Comp m c e) where
  pure :: forall a. a -> Comp m c e a
  pure x = Comp \cxt -> pure (Ok (Tuple cxt x))

instance bindComp :: Monad m => Bind (Comp m c e) where
  bind :: forall a b. Comp m c e a -> (a -> Comp m c e b) -> Comp m c e b
  bind (Comp x) f =
    Comp \cxt ->
      do
        res <- x cxt
        case res of
          Ok (Tuple cxt' x') ->
            do
              let Comp g = f x'
              g cxt'

          Error e ->
            pure $ Error e

instance monadComp :: Monad m => Monad (Comp m c a)

instance monadStateComp :: Monad m => MonadState c (Comp m c e) where
  state :: forall a. (c -> Tuple a c) -> Comp m c e a
  state f = Comp \cxt -> pure (Ok (Tuple.swap $ f cxt))

instance monadThrowComp :: Monad m => MonadThrow e (Comp m c e) where
  throwError :: forall a. e -> Comp m c e a
  throwError e = Comp \_ -> pure (Error e)

instance monadErrorComp :: Monad m => MonadError e (Comp m c e) where
  catchError :: forall a. Comp m c e a -> (e -> Comp m c e a) -> Comp m c e a
  catchError (Comp f) handle =
    Comp \cxt ->
      do
        res <- f cxt
        case res of
          Ok _ ->
            pure res

          Error err ->
            let Comp g = handle err
            in g cxt

instance bifunctorComp :: Monad m => Bifunctor (Comp m c) where
  bimap
    :: forall e1 e2 a1 a2
    . (e1 -> e2)
    -> (a1 -> a2)
    -> Comp m c e1 a1
    -> Comp m c e2 a2
  bimap f g (Comp h) =
    Comp \cxt ->
      do
        res <- h cxt
        pure $ bimap f (map g) res

liftResult :: forall m e a. MonadThrow e m => Result e a -> m a
liftResult = Result.toEither >>> liftEither

instance MonadEffect (Comp Effect c e) where
  liftEffect :: forall a. Effect a -> Comp Effect c e a
  liftEffect ex = Comp \c -> Ok <$> Tuple c <$> ex

instance Monoid c => MonadEffect (Comp Aff c e ) where
  liftEffect :: forall a. Effect a -> Comp Aff c e a
  liftEffect ex = Comp \c -> pure <$> pure <$> liftEffect ex

instance Monoid c => MonadAff (Comp Aff c e) where
  liftAff :: forall a. Aff a -> Comp Aff c e a
  liftAff ax = Comp \c -> Ok <$> Tuple c <$> ax
