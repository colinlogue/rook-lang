module Main where

import Prelude

import Data.List (List(..), (:))
import Effect (Effect)
import Node.Process (chdir)
import Rook.Compile.Target.JS (fileToJS)
import Rook.Pass.Full (runCompilation)



main :: Effect Unit
main = do
  -- let main' = "Main" : Nil
  -- let files = [ main' ]
  -- let root = "examples/ex01"
  -- runCompilation { files, root, main: main' }
  chdir "examples/ex03"
  fileToJS ("List" : Nil)

