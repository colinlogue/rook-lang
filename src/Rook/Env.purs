module Rook.Env where

-- import Prelude
-- import Rook.Syntax

-- import Data.Map (Map)
-- import Data.Map as Map
-- import Data.Maybe (Maybe(..))
-- import Data.Maybe as Maybe
-- import Data.Set (Set)
-- import Data.Stack (Stack)
-- import Data.Stack as Stack
-- import Data.Tuple (Tuple(..))
-- import Data.Tuple as Tuple

-- data Visibility
--   = Exported
--   | Private

-- type Definition =
--   { type :: RookType Unit
--   , expr :: Expr Unit
--   , visibility :: Visibility
--   }

-- type ModuleDef =
--   { definitions :: Map String Definition
--   }

-- type Env =
--   { modules :: Map ModulePath ModuleImport
--   }

-- data FrameType
--   = Block
--   | NamedFunction String
--   | AnonFunction

-- type Frame a =
--   { vals :: Stack (Tuple String a)
--   , type :: FrameType
--   }

-- checkFrame :: forall a. String -> Frame a -> Maybe Int
-- checkFrame name { vals } = Stack.locateBy (Tuple.fst >>> eq name) vals

-- type Env a =
--   { globals :: Map Path a
--   , locals :: Stack (Frame a)
--   }

-- -- does not allow shadowing
-- bindGlobal :: forall a. Path -> a -> Env a -> Maybe (Env a)
-- bindGlobal name val env =
--   if
--     Map.member name env.globals
--   then
--     Nothing
--   else
--     Just env { globals = Map.insert name val env.globals }

-- getBindingDistance :: forall a. String -> Env a -> Maybe BindingDistance
-- getBindingDistance ident { locals } = go 0 locals
--   where
--     go :: Int -> Stack (Frame a) -> Maybe BindingDistance
--     go i stack =
--       case Stack.pop stack of
--         Just (Tuple rest top) ->
--           case checkFrame ident top of
--             Just j ->
--               Just { depth: i, pos: j }

--             Nothing ->
--               go (i + 1) rest

--         Nothing ->
--           Nothing

-- bindLocal :: forall a. String -> a -> Env a -> Maybe (Env a)
-- bindLocal name x env =
--   case Stack.pop env.locals of
--     Just (Tuple rest top) ->
--       if
--         Maybe.isNothing $ checkFrame name top
--       then
--         Just $ env
--           { locals =
--               Stack.push
--                 (top { vals = Stack.push (Tuple name x) top.vals })
--                 rest
--           }

--       else
--         Nothing

--     Nothing ->
--       Nothing

-- addFrame :: forall a. FrameType -> Env a -> Env a
-- addFrame frameType env =
--   env
--     { locals =
--         Stack.push
--           { vals : Stack.empty
--           , type : frameType
--           }
--         env.locals
--     }

-- empty :: forall a. Env a
-- empty =
--   { globals : Map.empty
--   , locals : Stack.empty
--   }

-- lookup :: forall a. Name -> Env a -> Maybe a
-- lookup name env =
--   case name of
--     Global path ->
--       Map.lookup path env.globals

--     Local { depth, pos } ->
--       do
--         { vals } <- Stack.peekAt depth env.locals
--         Tuple.snd <$> Stack.peekAt pos vals

-- updateLocal :: forall a. BindingDistance -> a -> Env a -> Maybe (Env a)
-- updateLocal dist val env =
--   do
--     when (dist.depth < 0) Nothing
--     when (dist.pos < 0) Nothing
--     (Tuple stack frame) <- Stack.pop env.locals
--     if
--       dist.depth == 0
--     then do
--       { earlier, current, later } <- Stack.popMany dist.pos frame.vals
--       let Tuple name _ = current
--       let updated = Tuple name val
--       let vals = Stack.fromView { earlier, current : updated, later }
--       let frame' = frame { vals = vals }
--       Just env { locals = Stack.push frame' stack }
--     else
--       updateLocal (dist { depth = dist.depth - 1 }) val (env { locals = stack })
