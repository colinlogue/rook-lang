module Rook.Parse where

import Prelude
import Rook.Parse.Token
import Rook.Syntax.Common
import Rook.Syntax.Pattern
import Rook.Syntax.Surface

import Control.Alt ((<|>))
import Control.Lazy (defer)
import Data.Array as Array
import Data.Foldable (class Foldable, foldMap, foldl, foldr, oneOf, sum)
import Data.Function (applyFlipped)
import Data.List (List(..), many, (:))
import Data.List as List
import Data.Maybe (Maybe(..), fromMaybe, optional)
import Data.String.CodeUnits as String
import Data.Traversable (traverse)
import Data.Tuple (Tuple(..))
import Data.Tuple as Tuple
import Partial.Unsafe (unsafeCrashWith)
import Rook.Syntax.Common as Common
import Rook.Syntax.Literal (Literal(..))
import Rook.Syntax.Path (Path)
import Rook.Syntax.Pattern as Pattern
import Rook.Syntax.Surface as Surface
import Rook.Syntax.Surface as Syntax
import Text.Parsing.Parser (fail, position)
import Text.Parsing.Parser.Combinators (lookAhead, notFollowedBy, sepBy, try, (<?>))
import Text.Parsing.Parser.Pos (Position)
import Text.Parsing.Parser.String (char)
import Text.Parsing.Parser.Token (upper)
import Text.Parsing.Parser.Token.Extra (lower)

type SourcePos = { start :: Position, end :: Position }

type FunParam = Surface.FunParam SourcePos
type MatchArm = Surface.MatchArm SourcePos
type Pattern = Pattern.Pattern SourcePos
type Expr = Surface.Expr SourcePos
type Stmt = Surface.Stmt SourcePos
type RookType = Common.RookType SourcePos
type CallArg = Surface.CallArg SourcePos
type Module = Surface.Module SourcePos
type FileModule = Surface.FileModule SourcePos
type Declaration = Surface.Declaration SourcePos
type Import = Common.Import SourcePos
type DefParam = Common.DefParam SourcePos

-- TODO: Change lexeme parser so that it gets the start and the end, rather
-- than wrapping each rule individually. This will keep the end from including
-- all the trailing whitespace.

-----------------
--    Paths    --
-----------------

stringFromChars :: forall f. Foldable f => f Char -> String
stringFromChars = Array.fromFoldable >>> String.fromCharArray

nonLexemeIdent :: RookParser String
nonLexemeIdent =
  do
    init <- identStart
    rest <- many identLetter
    let str = stringFromChars (init : rest)
    if
      Array.elem str reservedNames
    then
      fail ("reserved word " <> str)
    else
      pure str

nonLexemeLowerIdent :: RookParser String
nonLexemeLowerIdent =
  lookAhead lower *> nonLexemeIdent

nonLexemeUpperIdent :: RookParser String
nonLexemeUpperIdent =
  lookAhead upper *> nonLexemeIdent

allUpperPath :: RookParser Path
allUpperPath =
  lexeme (sepBy nonLexemeUpperIdent (char '.') >>= case _ of
    Nil -> fail "empty path"
    p -> pure p)

ctorPath :: RookParser Path
ctorPath = allUpperPath <?> "constructor path"

path :: RookParser Path
path =
  lexeme (sepBy nonLexemeIdent (char '.') >>= case _ of
    Nil -> fail "empty path"
    p -> pure p)
  <?> "path"

typePath :: RookParser Path
typePath = allUpperPath <?> "type path"

--------------------
--    Patterns    --
--------------------

lit :: RookParser Literal
lit =
  oneOf
    [ IntLit <$> intLit
    , StringLit <$> stringLit
    , BoolLit true <$ reserved "true"
    , BoolLit false <$ reserved "false"
    ]
    <?> "literal"

ctorPattern :: RookParser Pattern
ctorPattern =
  do
    start <- position
    ctor <- ctorPath
    args <- ctorArgs
    end <- position
    pure $ CtorPattern { start, end } ctor args
  where
    pattern' = defer \_ -> pattern

    ctorArgs =
      oneOf
        [ parens $ commaSep pattern'
        , pure Nil
        ]

wildcardPattern :: RookParser Pattern
wildcardPattern =
  do
    start <- position
    void $ symbol "_"
    end <- position
    pure $ WildcardPattern { start, end }

patternVar :: RookParser Pattern
patternVar =
  do
    start <- position
    x <- lowerIdent
    end <- position
    pure $ PatternVar { start, end } x

literalPattern :: RookParser Pattern
literalPattern =
  do
    start <- position
    l <- lit
    end <- position
    pure $ LiteralPattern { start, end } l

pattern :: RookParser Pattern
pattern =
  oneOf
    [ wildcardPattern
    , patternVar
    , ctorPattern
    , literalPattern
    ]
    <?> "pattern"
  where
    pattern' = defer \_ -> pattern

------------------------------------
--    Arguments and parameters    --
------------------------------------

argList :: RookParser (List CallArg)
argList = parens $ commaSep callArg
  where
    expr' = defer \_ -> expr

callArg :: RookParser CallArg
callArg =
  oneOf
    [ ExprArg <$> expr'
    , do
        start <- position
        placeholder
        end <- position
        pure $ PlaceholderArg { start, end }
    ]
  where
    expr' = defer \_ -> expr

paramList :: RookParser (List FunParam)
paramList = parens $ commaSep param

param :: RookParser FunParam
param = { pattern : _, annotation : _ } <$> pattern <*> optional typeAnn

typeAnn :: RookParser RookType
typeAnn =
  -- need to disambiguate : and :=
  (lookAhead (notFollowedBy equals) *> colon)
    *> rookType
    <?> "type annotation"

matchArm :: RookParser MatchArm
matchArm = { pattern : _, definition : _ } <$> pattern  <* bigArrow <*> expr'
  where
    expr' = defer \_ -> expr

-----------------------
--    Expressions    --
-----------------------

identExpr :: RookParser Expr
identExpr =
  do
    start <- position
    modPath <- many (try (nonLexemeUpperIdent <* char '.'))
    name <- ident
    end <- position
    pure $ Ident { start, end } modPath name

ctor :: RookParser Expr
ctor =
  do
    start <- position
    cp <- ctorPath
    end <- position
    pure $ Ctor { start, end } cp

  -- do
  --   start <- position

  --   end <- position
  --   pure $ xxx { start, end }

identOrCtor :: RookParser Expr
identOrCtor =
  do
    start <- position
    sepBy nonLexemeUpperIdent (lookAhead (char '.' <* upper) *> char '.') >>= case _ of
      Nil ->
        do
          x <- lowerIdent
          end <- position
          pure $ Ident { start, end } Nil x

      modPath ->
        oneOf
          [ do
              void $ lookAhead (char '.')
              void $ char '.'
              x <- lowerIdent
              end <- position
              pure $ Ident { start, end } modPath x
          , case modPath of
              Nil -> fail "empty constructor path"
              _ -> position >>= \end -> pure $ Ctor { start, end } modPath
          ]

propAccess :: RookParser Expr
propAccess =
  do
    e <- expr'
    start <- position
    x <- propertyName
    end <- position
    pure $ PropertyAccess e { start, end } x
  where
    expr' = defer \_ -> expr

    -- Note that this requires no space between the '.' and the name.
    propertyName = char '.' *> lowerIdent

funExpr :: RookParser Expr
funExpr =
  do
    start <- position
    Tuple ps t <- Tuple <$> paramList <*> optional typeAnn <|> Tuple <$> singleParam <*> pure Nothing
    bigArrow
    e <- expr'
    end <- position
    pure $ FunExpr { start, end } ps t e
  where
    expr' = defer \_ -> expr

    singleParam :: RookParser (List FunParam)
    singleParam =
      do
        ({ pattern : _, annotation : Nothing } >>> List.singleton) <$> pattern

litExpr :: RookParser Expr
litExpr =
  do
    start <- position
    l <- lit
    end <- position
    pure $ LitExpr { start, end } l

simpleExpr :: RookParser Expr
simpleExpr =
  oneOf
    [ parens expr'
    , identOrCtor <* notFollowedBy bigArrow
    , litExpr
    ]
  where
    expr' = defer \_ -> expr

-- | A compound expression is a simple expression followed by any number of
-- | property accesses and function calls.
compoundExpr :: RookParser Expr
compoundExpr =
  do
    e <- simpleExpr
    tails <- many tail
    pure $ foldl applyFlipped e tails
  where
    tail :: RookParser (Expr -> Expr)
    tail =
      position >>= \start ->
        oneOf
          [ do
              args <- defer \_ -> argList
              end <- position
              pure \e -> Call e { start, end } args
          , do
              void dot
              x <- lowerIdent
              end <- position
              pure \e -> PropertyAccess e { start, end } x
          ]

matchExpr :: RookParser Expr
matchExpr =
  do
    start <- position
    reserved "match"
    e <- expr'
    reserved "with"
    arms <- braces (commaSep matchArm)
    end <- position
    pure $ Match { start, end } e arms
  where
    expr' = defer \_ -> expr

blockExpr :: RookParser Expr
blockExpr =
  do
    start <- position
    Tuple s e <- braces (Tuple<$> stmtList <*> optional expr')
    end <- position
    pure $ BlockExpr { start, end } s e
  where
    expr' = defer \_ -> expr

ifExpr :: RookParser Expr
ifExpr =
  do
    start <- position
    reserved "if"
    e1 <- parens expr'
    e2 <- expr'
    e3 <- optional (reserved "else" *> expr')
    end <- position
    pure $ If { start, end } e1 e2 e3
  where
    expr' = defer \_ -> expr

expr :: RookParser Expr
expr =
  oneOf
    [ defer \_ -> try funExpr
    , defer \_ -> compoundExpr
    , defer \_ -> matchExpr
    , defer \_ -> blockExpr
    , defer \_ -> ifExpr
    ]

--------------------------
----    Statements    ----
--------------------------

stmt :: RookParser Stmt
stmt =
  oneOf
    [ breakStmt
    , continueStmt
    , defer \_ -> forStmt
    , defer \_ -> letStmt
    , defer \_ -> mutStmt
    , try $ defer \_ -> assignStmt
    , defer \_ -> whileStmt
    , defer \_ -> loopStmt
    , defer \_ -> exprStmt
    ]
    <?> "statement"

forStmt :: RookParser Stmt
forStmt =
  do
    start <- position
    reserved "for"
    p <- pattern
    reserved "of"
    e <- expr'
    s <- braces stmtList
    end <- position
    end <- position
    pure $ For { start, end } p e s
  where
    expr' = defer \_ -> expr
    stmt' = defer \_ -> stmt

terminatedStmt :: RookParser Stmt
terminatedStmt =
  do
    s <- stmt'
    if needsSemi s
    then semi *> pure s
    else pure s
  where
    stmt' = defer \_ -> stmt

    needsSemi :: Stmt -> Boolean
    needsSemi s =
      case s of
        For _ _ _ _ ->
          false

        While _ _ _ ->
          false

        Loop _ _ ->
          false

        ExprStmt e ->
          exprNeedsSemi e

        _ ->
          true

    exprNeedsSemi :: Expr -> Boolean
    exprNeedsSemi e =
      case e of
        BlockExpr _ _ _ ->
          false

        If _ _ e' Nothing ->
          exprNeedsSemi e'

        If _ _ _ (Just e') ->
          exprNeedsSemi e'

        FunExpr _ _ _ e' ->
          exprNeedsSemi e'

        Match _ _ _ ->
          false

        _ ->
          true

stmtList :: RookParser (List Stmt)
stmtList = many $ try terminatedStmt'
  where
    terminatedStmt' = defer \_ -> terminatedStmt

whileStmt :: RookParser Stmt
whileStmt =
  do
    start <- position
    reserved "while"
    e <- expr'
    s <- braces stmtList
    end <- position
    pure $ While { start, end } e s
  where
    expr' = defer \_ -> expr
    stmt' = defer \_ -> stmt

loopStmt :: RookParser Stmt
loopStmt =
  do
    start <- position
    reserved "loop"
    s <- braces stmtList
    end <- position
    pure $ Loop { start, end } s
  where
    stmt' = defer \_ -> stmt

letStmt :: RookParser Stmt
letStmt =
  do
    start <- position
    reserved "let"
    x <- lowerIdent
    t <- optional typeAnn
    equals
    e <- expr'
    end <- position
    pure $ DeclStmt { start, end } Let x t e
    <?> "let statement"
  where
    expr' = defer \_ -> expr

mutStmt :: RookParser Stmt
mutStmt =
  do
    start <- position
    reserved "mut"
    x <- lowerIdent
    t <- optional typeAnn
    assignmentOp
    e <- expr'
    end <- position
    pure $ DeclStmt { start, end } Mut x t e
    <?> "mut statement"
  where
    expr' = defer \_ -> expr

assignStmt :: RookParser Stmt
assignStmt =
  do
    start <- position
    p <- many (nonLexemeUpperIdent <* char '.')
    x <- lowerIdent
    assignmentOp
    e <- expr'
    end <- position
    pure $ Assign { start, end } p x e
  where
    expr' = defer \_ -> expr

breakStmt :: RookParser Stmt
breakStmt =
  do
    start <- position
    reserved "break"
    end <- position
    pure $ Break { start, end }

continueStmt :: RookParser Stmt
continueStmt =
  do
    start <- position
    reserved "continue"
    end <- position
    pure $ Continue { start, end }

exprStmt :: RookParser Stmt
exprStmt =
  ExprStmt
    <$> expr'
    <?> "expression statement"
  where
    expr' = defer \_ -> expr

---------------------
----    Types    ----
---------------------

forallType :: RookParser RookType
forallType =
  do
    start <- position
    ts <- braces (commaSep lowerIdent)
    t <- rookType'
    end <- position
    pure $ Forall { start, end } ts t
    <?> "forall type"
  where
    rookType' = defer \_ -> rookType

typeVar :: RookParser RookType
typeVar =
  do
    start <- position
    a <- lowerIdent
    end <- position
    pure $ TypeVar { start, end } a
    <?> "type variable"

typeApp :: RookParser RookType
typeApp =
  do
    start <- position
    t <- simpleType
    ts <- angles (commaSep rookType')
    end <- position
    pure $ TypeApp { start, end } t ts
    <?> "type application"
  where
    rookType' = defer \_ -> rookType

typeAlias :: RookParser RookType
typeAlias =
  do
    void $ lookAhead upper
    start <- position
    p <- typePath
    end <- position
    pure $ TypeAlias { start, end } p

arrow :: RookParser RookType
arrow =
  do
    start <- position
    dom <- (List.singleton <$> simpleType) <|> parens (commaSep rookType')
    smallArrow
    cod <- rookType'
    end <- position
    pure $ Arrow { start, end } dom cod
    <?> "arrow type"
  where
    rookType' = defer \_ -> rookType

simpleType :: RookParser RookType
simpleType =
  oneOf
    [ try $ parens rookType'
    , typeVar
    , typeAlias
    ]
  where
    rookType' = defer \_ -> rookType

rookType :: RookParser RookType
rookType =
  oneOf
    -- [ defer \_ -> forallType
    -- , try $ defer \_ -> arrow
    -- , try $ defer \_ -> typeApp
    [ defer \_ -> forallType
    , try $ defer \_ -> arrow
    , try $ defer \_ -> typeApp
    , try $ defer \_ -> arrow
    , typeVar
    , typeAlias
    ]
    <?> "type"

-------------------
--    Modules    --
-------------------

moduleImport :: RookParser Import
moduleImport =
  do
    start <- position
    reserved "import"
    p <- path
    qualified <- optional (reserved "as" *> upperIdent)
    names <- braces (commaSep name)
    end <- position
    pure $ { qualified, names, path : p, info : { start, end } }
    <?> "module import"
  where
    name = do
      start <- position
      n <- ident
      end <- position
      pure $ { name : n, info : { start, end } }

defParam :: RookParser DefParam
defParam =
  do
    start <- position
    name <- lowerIdent
    annotation <- typeAnn
    end <- position
    pure $ { name , annotation, info : { start, end } }

def :: RookParser Declaration
def =
  do
    start <- position
    reserved "def"
    vars <- brackets (commaSep lowerIdent) <|> pure Nil
    x <- lowerIdent
    p <- optional (parens (commaSep defParam))
    t <- typeAnn
    equals
    e <- expr
    end <- position
    pure $ Def { start, end } vars x p t e
    <?> "definition declaration"

mod :: RookParser Declaration
mod =
  do
    start <- position
    reserved "mod"
    x <- upperIdent
    Tuple imports declarations <- braces (Tuple <$> many moduleImport <*> many declaration)
    end <- position
    pure $ Mod { start, end } x { imports, declarations }
    <?> "module declaration"

declaration :: RookParser Declaration
declaration =
  oneOf
    [ def
    , defer \_ -> mod
    ]
    <?> "declaration"


fileModule :: RookParser FileModule
fileModule = file' Nothing

file' :: Maybe Path -> RookParser FileModule
file' filePath =
  do
    whitespace
    reserved "module"
    p <- checkPath
    colon
    imports <- many moduleImport
    declarations <- many declaration
    pure $ Tuple p { imports, declarations }
    <?> "file module"
  where
    checkPath :: RookParser Path
    checkPath =
      do
        p <- path
        case filePath of
          Just expected ->
            (when (p /= expected) $ fail "path does not match file name")
              *> pure p
          _ ->
            pure p

file :: Path -> RookParser FileModule
file p = file' $ Just p