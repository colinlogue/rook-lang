module Rook.Types where


import Prelude
import Rook.Syntax.Bound

import Control.Monad.Comp (Comp(..), runComp)
import Control.Monad.Error.Class (catchError, throwError, try)
import Control.Monad.Error.Extra (note)
import Control.Monad.State as State
import Data.Bifunctor (lmap)
import Data.Either (Either(..))
import Data.Foldable (fold, foldl, traverse_)
import Data.Generic.Rep (class Generic)
import Data.Identity (Identity)
import Data.List (List(..), (:))
import Data.List as List
import Data.Map (Map)
import Data.Map as Map
import Data.Maybe (Maybe(..))
import Data.Maybe as Maybe
import Data.Newtype (overF, unwrap)
import Data.Result (Result(..))
import Data.Set (Set)
import Data.Set as Set
import Data.Show.Generic (genericShow)
import Data.Traversable (sequence, traverse)
import Data.Tuple (Tuple(..))
import Data.Tuple as Tuple
import Data.Unfoldable (replicate)
import Debug as Debug
import Effect (Effect)
import Effect.Console as Console
import Effect.Unsafe (unsafePerformEffect)
import Partial.Unsafe (unsafeCrashWith)
import Record as Record
import Rook.Annotated (meta)
import Rook.Context (Context)
import Rook.Context as Context
import Rook.Syntax.Common (Kind, Mutability(..), PrimOp(..), PrimType(..), RookType(..))
import Rook.Syntax.Literal (Literal(..))
import Rook.Syntax.Path (Path)
import Rook.Syntax.Pattern (Pattern(..))
import Rook.Types.Unification (Substitution, TypeConstraint(..), subAll, unify)
import Text.Parsing.Parser.Pos (Position(..))

-- infix 4 consistent as ~=

type SourcePos = Position

type DataConstructor =
  { paramTypes :: List (RookType Unit)
  , name :: String
  }

type TypeConstructor =
  { kind :: Kind
  , dataConstructors :: List DataConstructor
  }

type TypeDef =
  { type :: RookType Unit
  , isMutable :: Boolean
  , position :: SourcePos
  }

type TypeContext =
  { context :: Context TypeDef
  -- , constructors :: Map Path (Map String TypeConstructor)
  , constraints :: List TypeConstraint
  , freshCount :: Int
  , typeVars :: Set String
  }



freshen :: String -> TC String
freshen x =
  do
    { typeVars } <- State.get
    if
      Set.member x typeVars
    then
      go
    else
      pure x
  where
    go :: TC String
    go =
      do
        k <- freshCount
        { typeVars } <- State.get
        let x' = x <> "_" <> show k
        if
          Set.member x' typeVars
        then
          bumpFreshCount *> go
        else
          pure x'

newTypeVar :: TC String
newTypeVar =
  do
    x <- freshen "t"
    State.modify_ \s -> s { typeVars = Set.insert x s.typeVars }
    pure x

emptyTypeContext :: TypeContext
emptyTypeContext =
  { context : Context.empty
  , constraints : Nil
  , freshCount : 0
  , typeVars : Set.empty
  }

type TC a = Comp Identity TypeContext TypeError a

runTC :: forall a. TypeContext -> TC a -> Result TypeError a
runTC cxt = runComp cxt >>> unwrap >>> map Tuple.snd


data TypeError
  = UnknownGlobal SourcePos Path String (Context TypeDef)
  | InvalidLocal SourcePos Int
  | InconsistentTypes SourcePos (RookType Unit) (RookType Unit)
  | InBlock SourcePos TypeError
  | SynthError SourcePos
  | NotMatchableType SourcePos (RookType Unit)
  | MatchArmsFailedToUnify SourcePos
  | CallToNonFunctionType SourcePos (RookType Unit)
  | WrongNumberOfFunctionArgs SourcePos
      { actual :: Int, expected :: Int }
  | Shadowing SourcePos String SourcePos
  | IfBranchMismatch SourcePos TypeError
  | IfSynthError SourcePos
  | UnificationError SourcePos (RookType Unit) (RookType Unit)

derive instance Eq TypeError
derive instance Generic TypeError _
instance Show TypeError where
  show x = genericShow x

context :: TC (Context TypeDef)
context = State.get <#> \s -> s.context

constraints :: TC (List TypeConstraint)
constraints = State.get <#> \s -> s.constraints

freshCount :: TC Int
freshCount = State.get <#> \s -> s.freshCount

bumpFreshCount :: TC Unit
bumpFreshCount = State.modify_ (\s@{freshCount : k} -> s { freshCount = k + 1 })

addConstraint :: SourcePos -> RookType _ -> RookType _ -> TC Unit
addConstraint pos t1 t2 =
  do
    let c = TypeConstraint (void t1) (void t2)
    cs <- constraints
    -- Check that the constraint set still unifies
    when (Maybe.isNothing $ unify (c : cs))
      $ throwError
      $ UnificationError pos (void t1) (void t2)
    State.modify_ (\s -> s { constraints = c : cs })

-- typeContructors :: TC (Map Path (Map String TypeConstructor))
-- typeContructors = State.get <#> Record.get (Proxy :: Proxy "constructors")

scoped :: forall a. TC a -> TC a
scoped tc =
  do
    cxt <- context
    result <- tc
    State.modify_ (\s -> s { context = cxt })
    pure result

lookupGlobal :: SourcePos -> Path -> String -> TC (RookType Unit)
lookupGlobal pos path name =
  do
    cxt <- context
    case Context.lookup (Tuple path name) cxt of
      Just { type : t } -> pure t
      Nothing -> throwError $ UnknownGlobal pos path name cxt

lookupLocal :: SourcePos -> Int -> TC (RookType Unit)
lookupLocal pos i =
  do
    cxt <- context
    case Context.lookup i cxt of
      Just { type : t } -> pure t
      Nothing -> throwError $ InvalidLocal pos i

consistent :: Substitution -> RookType _ -> RookType _ -> Boolean
consistent subs lhs rhs = subAll subs lhs == subAll subs rhs

extend :: forall a. String -> Mutability -> SourcePos -> RookType a -> TC Unit
extend name dt pos t =
  do
    state <- State.get
    case Context.extend name x state.context of
      Ok cxt ->
        State.put state { context = cxt }

      Error (Context.Shadowing { position }) ->
        throwError
          $ Shadowing pos name position
  where
    x = { type : void t, isMutable : dt == Mut, position : pos }

applySubs :: RookType _ -> TC (RookType Unit)
applySubs t =
  do
    c <- constraints
    case unify c of
      Just s ->
        pure $ subAll s t

      Nothing ->
        unsafeCrashWith "this shouldn't happen"

checkConsistent :: SourcePos -> RookType _ -> RookType _ -> TC Unit
checkConsistent pos t1 t2 =
  do
    c <- constraints
    let b = unify c <#> \s -> subAll s t1 == subAll s t2
    unless (b == Just true)
      $ throwError
      $ InconsistentTypes pos t1 t2

checkExpr :: RookType _ -> Expr SourcePos -> TC Unit
checkExpr expected expr =
  case expr of
    _ ->
      do
        let pos = meta expr
        actual <- synthExpr expr
        addConstraint pos expected actual

checkStmt :: Stmt SourcePos -> TC Unit
checkStmt stmt =
  case stmt of
    DeclStmt pos dt name typeAnn def ->
      do
        t <- case typeAnn of
          Just t -> checkExpr (void t) def *> pure (void t)
          Nothing -> synthExpr def
        extend name dt pos t

    AssignGlobal pos path name expr ->
      do
        t <- lookupGlobal pos path name
        checkExpr t expr

    AssignLocal pos k expr ->
      do
        t <- lookupLocal pos k
        checkExpr t expr

    For _ pat expr stmts ->
      scoped do
        cxt <- State.get
        t1 <- synthExpr expr
        bindPattern pat
        -- TODO: Also check that t1 is iterable and pattern matches t1.
        traverse_ checkStmt stmts
        State.put cxt

    While _ expr stmts ->
      do
        cxt <- State.get
        checkExpr (Primitive unit BoolType) expr
        traverse_ checkStmt stmts
        State.put cxt

    Loop _ stmts ->
      do
        cxt <- State.get
        traverse_ checkStmt stmts
        State.put cxt

    Break _ ->
      pure unit

    Continue _ ->
      pure unit

    ExprStmt expr ->
      checkExpr (Primitive unit UnitType) expr

    AssignRef _ _ _ ->
      unsafeCrashWith "references should not appear until after typechecking"

synthExpr :: Expr SourcePos -> TC (RookType Unit)
synthExpr expr =
  applySubs =<< case expr of
    Global pos path name ->
      lookupGlobal pos path name

    Local pos i ->
      lookupLocal pos i

    Ctor _ _ ->
      unsafeCrashWith "synthExpr not implemented for constructors"

    PropertyAccess e pos name ->
      unsafeCrashWith "synthExpr not implemented for property access"

    BlockExpr pos stmts final ->
      do
        cxt <- State.get
        traverse_ checkStmt stmts
        t <- case final of
          Just e ->
            synthExpr e

          Nothing ->
            pure $ Primitive unit UnitType

        State.put cxt
        pure t

    If _ guard thenBranch Nothing ->
      do
        checkExpr (Primitive unit BoolType) guard
        checkExpr (Primitive unit UnitType) thenBranch
        pure $ Primitive unit UnitType

    If _ guard thenBranch (Just elseBranch) ->
      do
        checkExpr (Primitive unit BoolType) guard
        res <- try $ synthExpr thenBranch
        case res of
          Right t ->
            checkElseBranch t

          Left (SynthError pos) ->
            do
              t <- synthExpr elseBranch
                `catchError` \_ -> throwError $ IfSynthError pos
              checkElseBranch t

          Left err ->
            throwError err

      where
        checkElseBranch :: RookType Unit -> TC (RookType Unit)
        checkElseBranch t = do
          checkExpr t elseBranch
            `catchError` \e -> throwError $ IfBranchMismatch (meta thenBranch) e
          pure t

    FunExpr pos params codAnn body ->
      do
        -- Save the context.
        cxt <- context

        -- Add the parameter bindings to the context.
        Tuple newTypeVars paramTypes <-
          traverse bindParam params
            <#> map (map List.singleton)
            <#> fold

        returnType <- case codAnn of
          -- If the codomain is specified, check the body against it.
          Just cod ->
            checkExpr (void cod) body
              *> pure (void cod)

          -- If not, try to synthesize the codomain from the body.
          Nothing ->
            synthExpr body

        -- Restore the context without the parameter bindings.
        State.modify_ (\s -> s { context = cxt })

        -- If any new type variables were created, bind them with forall.
        let t = Arrow unit paramTypes returnType
        pure case newTypeVars of
          Nil -> t
          xs -> Forall unit xs t

      where
        bindParam
          :: { pattern :: Pattern SourcePos, annotation :: Maybe (RookType SourcePos) }
          -> TC (Tuple (List String) (RookType Unit))
        bindParam { pattern : pat, annotation : typeAnn } =
          case pat of
            PatternVar patPos name ->
              case typeAnn of
                Just t ->
                  do
                    extend name Let patPos t
                    pure $ Tuple Nil $ void t

                Nothing ->
                  do
                    tvar <- newTypeVar
                    let t = TypeVar unit tvar
                    extend name Let patPos t
                    pure $ Tuple (tvar : Nil) t

            LiteralPattern _ lit ->
              pure $ Tuple Nil $ Primitive unit $ litType lit

            CtorPattern _ _ _ ->
              -- TODO: Bind constructor pattern variables.
              throwError $ SynthError pos

            WildcardPattern patPos ->
              case typeAnn of
                Just t ->
                  pure $ Tuple Nil $ void t

                Nothing ->
                  do
                    tvar <- newTypeVar
                    let t = TypeVar unit tvar
                    -- Don't extend the context if no name is bound, but do
                    -- add a new type variable for unification.
                    pure $ Tuple (tvar : Nil) t

    Match pos scrutinee arms ->
      do
        t1 <- synthExpr scrutinee
        unless (isMatchableType t1)
          $ throwError
          $ NotMatchableType pos t1
        -- TODO: Check for exhaustive patterns and that all patterns are the
        -- correct type.
        armTypes <- traverse synthArm arms
        case List.uncons armTypes of
          Just { head, tail } ->
            foldl (\acc cur -> acc >>= unifier cur) (Just head) tail
              # note (MatchArmsFailedToUnify pos)
          Nothing ->
            pure $ Primitive unit NeverType

      where
        synthArm :: MatchArm SourcePos -> TC (RookType Unit)
        synthArm { pattern : pat, definition : arm } =
          do
            cxt <- State.get
            bindPattern pat
            t <- synthExpr arm
            State.put cxt
            pure t

    LitExpr _ lit ->
      pure $ Primitive unit $ litType lit

    PrimOpExpr _ op ->
      pure $ primOpType op

    Call func pos args ->
      synthExpr func >>= go Nil
      where
        go :: List String -> RookType _ -> TC (RookType Unit)
        go xs =
          case _ of
            Arrow _ paramTypes returnType ->
              do
                let expected = List.length paramTypes
                let actual = List.length args
                unless (expected == actual)
                  $ throwError
                  $ WrongNumberOfFunctionArgs pos { expected, actual }
                remainingParams <- List.catMaybes
                  <$> (traverse checkParam $ List.zip args paramTypes)
                case remainingParams of
                  Nil ->
                    pure $ void returnType

                  _ ->
                    pure $ Arrow unit remainingParams (void returnType)

              where
                checkParam :: Tuple (CallArg Position) (RookType Unit) -> TC (Maybe (RookType Unit))
                checkParam (Tuple callArg paramType) =
                  case callArg of
                    ExprArg arg ->
                      checkExpr paramType arg
                        *> pure Nothing

                    PlaceholderArg pos ->
                      pure $ Just paramType

            TypeVar _ x ->
              do
                Tuple newTypeVars paramTypes <- List.unzip <$> traverse synthArg args
                returnType <- TypeVar unit <$> newTypeVar
                let t = Arrow unit paramTypes returnType
                addConstraint pos (TypeVar unit x) $
                  case List.catMaybes newTypeVars of
                    Nil -> t
                    ts -> Forall unit ts t
                pure returnType
              where
                synthArg :: CallArg SourcePos -> TC (Tuple (Maybe String) (RookType Unit))
                synthArg a =
                  case a of
                    ExprArg arg ->
                      synthExpr arg <#> Tuple Nothing

                    PlaceholderArg _ ->
                      do
                        tvar <- newTypeVar
                        pure $ Tuple (Just tvar) (TypeVar unit tvar)


            Forall _ ys u ->
              go (xs <> ys) u

            t ->
              throwError $ CallToNonFunctionType pos t

    RefExpr _ _ ->
      unsafeCrashWith "ref expressions shouldn't appear until after typechecking"

primOpType :: PrimOp -> RookType Unit
primOpType = case _ of
  Print -> Arrow unit (Primitive unit StringType : Nil) (Primitive unit UnitType)

litType :: Literal -> PrimType
litType lit =
  case lit of
    IntLit _ -> IntType
    StringLit _ -> StringType
    BoolLit _ -> BoolType
    UnitLit -> UnitType

isMatchableType :: RookType _ -> Boolean
isMatchableType t =
  case t of
    Primitive _ _ ->
      true

    -- TODO
    _ ->
      false

isIterableType :: RookType _ -> Boolean
isIterableType t = false --TODO

bindPattern :: Pattern _ -> TC Unit
bindPattern pat =
  case pat of
    _ -> unsafeCrashWith "to do: bindPattern"

unifier :: RookType _ -> RookType _ -> Maybe (RookType Unit)
unifier t1 t2 =
  do
    s <- unify (TypeConstraint t1 t2 : Nil)
    pure $ subAll s t1
