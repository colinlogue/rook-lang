module Rook.Interp where

import Prelude

import Control.Monad.Comp (Comp, liftResult, runComp)
import Control.Monad.Error.Class (throwError)
import Control.Monad.State.Class as State
import Data.Array as Array
import Data.Either (Either(..))
import Data.Either.Extra as Either
import Data.Foldable (foldMap, foldr)
import Data.List (List(..), (:))
import Data.List as List
import Data.Map (Map)
import Data.Map as Map
import Data.Maybe (Maybe(..))
import Data.Result (Result(..))
import Data.Result as Result
import Data.Set (Set)
import Data.Stack (Stack)
import Data.Stack as Stack
import Data.Traversable (sequence, traverse, traverse_)
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Class (liftEffect)
import Effect.Class.Console as Console
import Partial.Unsafe (unsafeCrashWith)
import Rook.Syntax.Bound (CallArg(..), Expr(..), Stmt(..))
import Rook.Syntax.Common (PrimOp(..), Reference)
import Rook.Syntax.Literal (Literal(..))
import Rook.Syntax.Path (Path)
import Rook.Syntax.Pattern (Pattern)
import Rook.Syntax.Pattern as Pattern
import Rook.Syntax.Value (Value(..))

type CallStack = Stack FrameRef

type FrameRef = Int

type Memory = Map FrameRef Frame

type Frame = Array RefCell

type RefCell =
  { value :: Maybe Value
  , name :: String
  }

type InterpState =
  { memory :: Memory
  , callStack :: CallStack
  , nextRef :: Int
  , assignments :: Map String Reference
  , globals :: Map Path (Map String Value)
  }

initialState :: Map Path (Map String Value) -> InterpState
initialState globals =
  { memory : Map.empty
  , callStack : Stack.empty
  , nextRef : 0
  , assignments : Map.empty
  , globals
  }

type Interp a = Comp Effect InterpState String a

interp :: InterpState -> Expr _ -> Effect Value
interp s e =
  do
    result <- runComp s $ interpExpr e
    case result of
      Ok (Tuple _ val) ->
        pure val

      Error err ->
        unsafeCrashWith $ "error: " <> show err


openFrame :: List String -> Interp Unit
openFrame xs = newFrame xs >>= pushFrame

closeFrame :: Interp Unit
closeFrame =
  do
    { callStack } <- State.get
    case Stack.pop callStack of
      Just (Tuple remStack _) ->
        State.modify_ \s -> s { callStack = remStack }

      Nothing ->
        throwError "popped from empty stack"

-- push an already existing frame to the stack
pushFrame :: FrameRef -> Interp Unit
pushFrame ref =
  State.modify_ \s -> s { callStack = Stack.push ref s.callStack }

-- build a new frame but don't put it on the stack
newFrame :: List String -> Interp FrameRef
newFrame xs =
  do
    { memory, nextRef } <- State.get
    let frame = Array.fromFoldable $ { name : _, value : Nothing } <$> xs
    State.modify_ \s -> s
      { memory = Map.insert nextRef frame memory
      , nextRef = nextRef + 1
      }
    pure nextRef

currentFrame :: Interp FrameRef
currentFrame =
  State.get >>= \{ callStack } ->
    case Stack.peek callStack of
      Just ref -> pure ref
      Nothing -> throwError "call stack is empty"

getLocal :: Int -> Interp Reference
getLocal i =
  do
    { callStack } <- State.get
    go callStack i
  where
    go :: CallStack -> Int -> Interp Reference
    go callStack index =
      case Stack.pop callStack of
        Just (Tuple remStack top) ->
          getFrame top >>= \frame ->
            let index' = index - Array.length frame
            in
              if
                index' < 0
              then
                pure { frame : top, index }
              else
                go remStack index'

        Nothing ->
          throwError $ "local out of bounds: " <> show i

getFrame :: FrameRef -> Interp Frame
getFrame frameRef =
  do
    { memory } <- State.get
    case Map.lookup frameRef memory of
      Just frame ->
        pure frame

      Nothing ->
        throwError $ "invalid frame ref: " <> show frameRef

getName :: String -> Interp Reference
getName x =
  do
    { assignments } <- State.get
    case Map.lookup x assignments of
      Just ref ->
        pure ref

      Nothing ->
        throwError
          $ "unknown name: " <> x

lookupGlobal :: Path -> String -> Interp Value
lookupGlobal path name =
  do
    { globals } <- State.get
    mod <- Map.lookup path globals
      # Result.note ("unknown module: " <> show path <> "\n" <> show globals)
      # liftResult
    Map.lookup name mod
      # Result.note ("unknown name " <> name <> " in module " <> show path)
      # liftResult

lookupRef :: Reference -> Interp Value
lookupRef { frame : frameRef, index } =
  do
    { memory } <- State.get
    frame <- Map.lookup frameRef memory
      # Result.note "frame out of bounds"
      # liftResult
    Array.index frame index
      # Result.note "index out of bounds"
      <#> (\x -> x.value)
      <#> Result.note "value has not been initialized"
      # join
      # liftResult

assign :: Reference -> Value -> Interp (Maybe Control)
assign { frame, index } val =
  do
    { memory } <- State.get
    case Map.lookup frame memory of
      Just frameVals ->
        case Array.modifyAt index (\x -> x { value = Just val }) frameVals of
          Just frameVals' ->
            State.modify_ \s -> s
              { memory = Map.insert frame frameVals' memory }

          Nothing ->
            throwError "index out of bounds"

      Nothing ->
        throwError "frame out of bounds"
    pure Nothing

interpExpr :: Expr _ -> Interp Value
interpExpr = case _ of
  Global _ path name ->
    lookupGlobal path name

  Local _ i ->
    getLocal i >>= lookupRef

  RefExpr _ ref ->
    lookupRef ref

  Ctor _ _ ->
    unsafeCrashWith "constructors not supported yet"

  PropertyAccess _ _ _ ->
    unsafeCrashWith "property access not supported"

  FunExpr _ params _ e ->
    pure $ FunVal (params <#> \x -> x.pattern) e

  Call e _ args ->
    do
      f <- interpExpr e
      case f of
        FunVal params body ->
          do
            { params, body } <- asFunValue f
            Tuple remainingParams argVals <- Either.separate
              <$> (traverse interpArg $ List.zip params args)
            case remainingParams of
              Nil ->
                do
                  let frameVars = foldMap Pattern.bindings params
                  openFrame frameVars
                  val <- interpExpr body
                  closeFrame
                  pure val

              _ ->
                throwError "call placeholder args not supported yet"

        PrimOpVal op ->
          case op of
            Print ->
              do
                args' <- traverse checkArg args
                argVals <- traverse interpExpr args'
                case argVals of
                  Cons (LitVal (StringLit s)) Nil ->
                    do
                      liftEffect $ Console.log s
                      pure $ LitVal UnitLit

                  _ ->
                    throwError "invalid arguments to print"
              where
                checkArg :: CallArg _ -> Interp (Expr _)
                checkArg = case _ of
                  ExprArg e -> pure e
                  PlaceholderArg _ -> throwError $ "placeholders not supported"

        _ ->
          throwError $ "call to non-function value: " <> show f
    where
      interpArg :: Tuple (Pattern _) (CallArg _) -> Interp (Either (Pattern Unit) Value)
      interpArg (Tuple pat arg) =
        case arg of
          ExprArg e' ->
            Right <$> interpExpr e'

          PlaceholderArg _ ->
            pure $ Left pat

      asFunValue :: Value -> Interp { params :: List (Pattern Unit) , body :: Expr Unit }
      asFunValue val =
        case val of
          FunVal params body ->
            pure { params, body }

          _ ->
            throwError $ "call to non-function value: " <> show val

  LitExpr _ lit ->
    pure $ LitVal lit

  BlockExpr _ stmts final ->
    do
      let blockVars = getDeclarations stmts
      openFrame blockVars
      -- TODO: What happens if there is a break in a block expression?
      void $ interpBlock stmts
      val <- case final of
        Just e ->
          interpExpr e

        Nothing ->
          pure $ LitVal UnitLit
      closeFrame
      pure val

  Match _ _ _ ->
    unsafeCrashWith "match not implemented"

  If _ e1 e2 e3 ->
    do
      v1 <- interpExpr e1
      case v1 of
        LitVal (BoolLit b) ->
          if
            b
          then
            interpExpr e2
          else do
            e3' <- e3 # Result.note "missing else clause" # liftResult
            interpExpr e3'

        _ ->
          throwError "if guard not a boolean value"

  PrimOpExpr _ op ->
    pure $ PrimOpVal op

data Control
  = BreakControl
  | ContinueControl

interpStmt :: Stmt _ -> Interp (Maybe Control)
interpStmt = case _ of
  DeclStmt _ _ x _ e ->
    do
      val <- interpExpr e
      ref <- getName x
      assign ref val

  AssignGlobal _ _ _ _ ->
    throwError "assigning to globals no permitted"

  AssignLocal _ i e ->
    do
      val <- interpExpr e
      ref <- getLocal i
      assign ref val

  AssignRef _ ref e ->
    do
      val <- interpExpr e
      assign ref val

  -- For _ pat e stmts ->

  While pos e stmts ->
    do
      let frameVars = getDeclarations stmts
      frameRef <- newFrame frameVars
      loop frameRef
    where
      loop :: FrameRef -> Interp (Maybe Control)
      loop frameRef = do
        val <- interpExpr e
        case val of
          LitVal (BoolLit b) ->
            if
              b
            then do
              pushFrame frameRef
              ctrl <- interpBlock stmts
              closeFrame
              case ctrl of
                Nothing ->
                  loop frameRef

                Just BreakControl ->
                  pure Nothing

                Just ContinueControl ->
                  loop frameRef
            else
              pure Nothing

          _ ->
            throwError
              $ "while block guard not a boolean: " <> show val

  Loop _ stmts ->
    do
      let frameVars = getDeclarations stmts
      openFrame frameVars
      loop
      closeFrame
      pure Nothing
    where
      loop :: Interp Unit
      loop =
        do
          ctrl <- interpBlock stmts
          case ctrl of
            Nothing ->
              loop

            Just BreakControl ->
              pure unit

            Just ContinueControl ->
              loop

  Break _ ->
    pure $ Just BreakControl

  Continue _ ->
    pure $ Just ContinueControl

  ExprStmt e ->
    interpExpr e *> pure Nothing

  _ ->
    unsafeCrashWith "not implemented"


getDeclarations :: List (Stmt _) -> List String
getDeclarations = foldr collect Nil
  where
    collect :: Stmt _ -> List String -> List String
    collect s acc = case s of
      DeclStmt _ _ name _ _ ->
        name : acc

      _ ->
        acc

interpBlock :: List (Stmt _) -> Interp (Maybe Control)
interpBlock = case _ of
  Nil ->
    pure Nothing

  Cons s rest ->
    do
      ctrl <- interpStmt s
      case ctrl of
        Nothing ->
          interpBlock rest

        Just c ->
          pure $ Just c
