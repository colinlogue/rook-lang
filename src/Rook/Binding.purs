module Rook.Binding where

import Prelude

import Data.Foldable (foldl, foldr)
import Data.List (List(..), (:))
import Data.List as List
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Tuple (Tuple(..))
import Rook.Syntax.Bound as Bound
import Rook.Syntax.Pattern as Pattern
import Rook.Syntax.Surface as Surface


captureModuleVars :: forall a. Surface.Module a -> Bound.Module a
captureModuleVars { imports, declarations } =
  { imports
  , declarations : captureDeclarationVars <$> declarations
  }

captureDeclarationVars :: forall a. Surface.Declaration a -> Bound.Declaration a
captureDeclarationVars = case _ of
  Surface.Def pos typeVars name params t e ->
    let
      xs = foldr (\p acc -> p.name : acc) Nil <$> params
      e' = captureExprVars (fromMaybe Nil xs) e
    in
      Bound.Def pos typeVars name params t e'

  Surface.Mod pos name mod ->
    Bound.Mod pos name $ captureModuleVars mod


captureExprVars :: forall a. List String -> Surface.Expr a -> Bound.Expr a
captureExprVars xs =
  case _ of
    Surface.Ident pos path name ->
      case List.findIndex (eq name) xs of
        Just i ->
          Bound.Local pos i

        Nothing ->
          Bound.Global pos path name

    Surface.Ctor pos path ->
      Bound.Ctor pos path

    Surface.PropertyAccess e pos x ->
      let
        e' = captureExprVars xs e
      in
        Bound.PropertyAccess e' pos x

    Surface.FunExpr pos params t e ->
      let
        xs' = foldl (\acc { pattern } -> Pattern.bindings pattern <> acc) xs params
        e' = captureExprVars xs' e
      in
        Bound.FunExpr pos params t e'

    Surface.Call e pos args ->
      let
        e' = captureExprVars xs e
        args' = map (captureArgVars xs) args
      in
        Bound.Call e' pos args'

    Surface.LitExpr pos lit ->
      Bound.LitExpr pos lit

    Surface.BlockExpr pos stmts final ->
      let
        Tuple stmts' final' = captureBlockVars xs stmts final
      in
        Bound.BlockExpr pos stmts' final'

    Surface.Match pos e arms ->
      let
        e' = captureExprVars xs e
        arms' = map (captureArmVars xs) arms
      in
        Bound.Match pos e' arms'

    Surface.If pos e1 e2 e3 ->
      let
        e1' = captureExprVars xs e1
        e2' = captureExprVars xs e2
        e3' = captureExprVars xs <$> e3
      in
        Bound.If pos e1' e2' e3'

captureArmVars :: forall a. List String -> Surface.MatchArm a -> Bound.MatchArm a
captureArmVars xs { pattern, definition } =
  let
    xs' = Pattern.bindings pattern <> xs
  in
    { pattern, definition : captureExprVars xs' definition }

captureBlockVars
  :: forall a
  . List String
  -> List
  (Surface.Stmt a)
  -> Maybe (Surface.Expr a)
  -> Tuple (List (Bound.Stmt a)) (Maybe (Bound.Expr a))
captureBlockVars xs stmts e =
  let
    collect (Tuple ys acc) cur = captureStmtVars ys cur <#> \cur' -> cur' : acc
    Tuple xs' stmts' = List.reverse <$> foldl collect (Tuple xs Nil) stmts
    e' = captureExprVars xs' <$> e
  in
    Tuple stmts' e'


captureStmtVars :: forall a. List String -> Surface.Stmt a -> Tuple (List String) (Bound.Stmt a)
captureStmtVars xs =
  case _ of
    Surface.DeclStmt pos mut name t e ->
      let
        e' = captureExprVars xs e
        xs' = name : xs
      in
        Tuple xs' $ Bound.DeclStmt pos mut name t e'

    Surface.Assign pos path name e ->
      let
        e' = captureExprVars xs e
      in
        Tuple xs $ case List.findIndex (eq name) xs of
          Just i ->
            Bound.AssignLocal pos i e'

          Nothing ->
            Bound.AssignGlobal pos path name e'

    Surface.For pos pat e stmts ->
      let
        e' = captureExprVars xs e
        xs' = Pattern.bindings pat <> xs
        Tuple stmts' _ = captureBlockVars xs' stmts Nothing
      in
        Tuple xs $ Bound.For pos pat e' stmts'

    Surface.While pos e stmts ->
      let
        e' = captureExprVars xs e
        Tuple stmts' _ = captureBlockVars xs stmts Nothing
      in
        Tuple xs $ Bound.While pos e' stmts'

    Surface.Loop pos stmts ->
      let
        Tuple stmts' _ = captureBlockVars xs stmts Nothing
      in
        Tuple xs $ Bound.Loop pos stmts'

    Surface.Break pos ->
      Tuple xs $ Bound.Break pos

    Surface.Continue pos ->
      Tuple xs $ Bound.Continue pos

    Surface.ExprStmt e ->
      let
        e' = captureExprVars xs e
      in
        Tuple xs $ Bound.ExprStmt e'



captureArgVars :: forall a. List String -> Surface.CallArg a -> Bound.CallArg a
captureArgVars xs =
  case _ of
    Surface.ExprArg e ->
      Bound.ExprArg $ captureExprVars xs e

    Surface.PlaceholderArg pos ->
      Bound.PlaceholderArg pos

