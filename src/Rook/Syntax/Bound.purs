module Rook.Syntax.Bound where

import Prelude

import Data.Generic.Rep (class Generic)
import Data.List (List)
import Data.Maybe (Maybe)
import Data.Show.Generic (genericShow)
import Rook.Annotated (class Annotated)
import Rook.Syntax.Common (DefParam, FunParam, Import, Mutability, PrimOp, Reference, RookType)
import Rook.Syntax.Literal (Literal)
import Rook.Syntax.Path (Path)
import Rook.Syntax.Pattern (Pattern)


data CallArg pos
  = ExprArg (Expr pos)
  | PlaceholderArg pos

derive instance Eq a => Eq (CallArg a)
derive instance Generic (CallArg a) _
derive instance Functor CallArg

instance Show a => Show (CallArg a) where
  show x = genericShow x

type MatchArm pos =
  { pattern :: Pattern pos
  , definition :: Expr pos
  }

data Expr pos
  = Global pos Path String
  | Local pos Int
  | RefExpr pos Reference
  | Ctor pos Path
  | PropertyAccess (Expr pos) pos String
  | FunExpr pos (List (FunParam pos)) (Maybe (RookType pos)) (Expr pos)
  | Call (Expr pos) pos (List (CallArg pos))
  | LitExpr pos Literal
  | BlockExpr pos (List (Stmt pos)) (Maybe (Expr pos))
  | Match pos (Expr pos) (List (MatchArm pos))
  | If pos (Expr pos) (Expr pos) (Maybe (Expr pos))
  | PrimOpExpr pos PrimOp

derive instance Eq a => Eq (Expr a)
derive instance Functor Expr
derive instance Generic (Expr a) _

instance showExpr :: Show a => Show (Expr a) where
  show x = genericShow x

data Stmt pos
  = DeclStmt pos Mutability String (Maybe (RookType pos)) (Expr pos)
  | AssignGlobal pos Path String (Expr pos)
  | AssignLocal pos Int (Expr pos)
  | AssignRef pos Reference (Expr pos)
  | For pos (Pattern pos) (Expr pos) (List (Stmt pos))
  | While pos (Expr pos) (List (Stmt pos))
  | Loop pos (List (Stmt pos))
  | Break pos
  | Continue pos
  | ExprStmt (Expr pos)

derive instance eqStmt :: Eq a => Eq (Stmt a)
derive instance functorStmt :: Functor Stmt
derive instance genericStmt :: Generic (Stmt a) _

instance showStmt :: Show a => Show (Stmt a) where
  show x = genericShow x

data Declaration a
  = Def a (List String) String (Maybe (List (DefParam a))) (RookType a) (Expr a)
  | Mod a String (Module a)

derive instance Eq a => Eq (Declaration a)
derive instance Generic (Declaration a) _
derive instance Functor Declaration

instance Show a => Show (Declaration a) where
  show x = genericShow x

type Module a =
  { imports :: List (Import a)
  , declarations :: List (Declaration a)
  }

instance Annotated Expr where
  meta = case _ of
    Global pos _ _ ->
      pos

    Local pos _ ->
      pos

    Ctor pos _ ->
      pos

    PropertyAccess _ pos _ ->
      pos

    FunExpr pos _ _ _ ->
      pos

    Call _ pos _ ->
      pos

    LitExpr pos _ ->
      pos

    BlockExpr pos _ _ ->
      pos

    Match pos _ _ ->
      pos

    If pos _ _ _ ->
      pos

    PrimOpExpr pos _ ->
      pos

    RefExpr pos _ ->
      pos