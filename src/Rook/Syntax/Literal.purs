module Rook.Syntax.Literal where

import Prelude

import Data.Generic.Rep (class Generic)
import Data.Show.Generic (genericShow)

data Literal
  = IntLit Int
  | StringLit String
  | BoolLit Boolean
  | UnitLit

derive instance eqLiteral :: Eq Literal
derive instance genericLiteral:: Generic Literal _

instance showLiteral :: Show Literal where
  show x = genericShow x
