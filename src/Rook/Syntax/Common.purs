module Rook.Syntax.Common where

import Prelude

import Data.Generic.Rep (class Generic)
import Data.List (List)
import Data.Maybe (Maybe)
import Data.Show.Generic (genericShow)
import Rook.Syntax.Path (Path)
import Rook.Syntax.Pattern (Pattern)

data Mutability
  = Let
  | Mut

derive instance Eq Mutability
derive instance Generic Mutability _

instance Show Mutability where

  show x = genericShow x

data PrimType
  = IntType
  | StringType
  | BoolType
  | UnitType
  | NeverType

derive instance eqPrimType :: Eq PrimType
derive instance genericPrimType :: Generic PrimType _

instance showPrimType:: Show PrimType where
  show x = genericShow x

data RookType pos
  = Forall pos (List String) (RookType pos)
  | TypeVar pos String
  | TypeAlias pos Path
  | Arrow pos (List (RookType pos)) (RookType pos)
  | TypeApp pos (RookType pos) (List (RookType pos))
  | Primitive pos PrimType

derive instance Eq a => Eq (RookType a)
derive instance Functor RookType
derive instance Generic (RookType a) _

instance Show a => Show (RookType a) where
  show x = genericShow x

data Kind
  = Star
  | StarArrow Kind

type DefParam a =
  { name :: String
  , annotation :: RookType a
  , info :: a
  }

type Import a =
  { qualified :: Maybe String
  , path :: Path
  , names :: List { name :: String, info :: a }
  , info :: a
  }

type Export a =
  { name :: String
  , info :: a
  }

type FunParam pos =
  { pattern :: Pattern pos
  , annotation :: Maybe (RookType pos)
  }

type Reference = { frame :: Int, index :: Int }

data PrimOp
  = Print

derive instance Eq PrimOp
derive instance Generic PrimOp _

instance Show PrimOp where
  show x = genericShow x
