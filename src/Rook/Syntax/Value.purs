module Rook.Syntax.Value where

import Prelude

import Data.Generic.Rep (class Generic)
import Data.List (List)
import Data.Maybe (Maybe(..))
import Data.Show.Generic (genericShow)
import Rook.Syntax.Bound as Bound
import Rook.Syntax.Common (PrimOp)
import Rook.Syntax.Literal (Literal)
import Rook.Syntax.Pattern (Pattern)

data Value
  = FunVal (List (Pattern Unit)) (Bound.Expr Unit)
  | LitVal Literal
  | PrimOpVal PrimOp

derive instance Generic Value _

instance Show Value where
  show x = genericShow x

toExpr :: Value -> Bound.Expr Unit
toExpr = case _ of
  PrimOpVal op -> Bound.PrimOpExpr unit op
  LitVal lit -> Bound.LitExpr unit lit
  FunVal pats e -> Bound.FunExpr unit ({pattern : _, annotation : Nothing} <$> pats) Nothing e