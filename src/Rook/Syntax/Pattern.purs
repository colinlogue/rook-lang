module Rook.Syntax.Pattern where

import Prelude

import Data.Foldable (foldMap, foldl)
import Data.Generic.Rep (class Generic)
import Data.List (List(..))
import Data.Show.Generic (genericShow)
import Rook.Syntax.Literal (Literal)
import Rook.Syntax.Path (Path)


data Pattern a
  = PatternVar a String
  | CtorPattern a Path (List (Pattern a))
  | LiteralPattern a Literal
  | WildcardPattern a


derive instance eqPattern :: Eq a => Eq (Pattern a)
derive instance genericPattern :: Generic (Pattern a) _
derive instance functorPattern :: Functor Pattern

instance showPattern :: Show a => Show (Pattern a) where
  show x = genericShow x


bindings :: forall a. Pattern a -> List String
bindings pat =
  case pat of
    PatternVar _ x ->
      pure x

    CtorPattern _ _ pats ->
      foldl (\acc cur -> bindings cur <> acc) Nil pats

    WildcardPattern _ ->
      Nil

    LiteralPattern _ _ ->
      Nil
