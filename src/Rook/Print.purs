module Rook.Print where

import Prelude
import Rook.Syntax.Bound

import Control.Lazy (defer)
import Data.Foldable (class Foldable, foldMap, intercalate)
import Data.FoldableWithIndex (foldrWithIndex)
import Data.List (List(..), foldl, (:))
import Data.List as List
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Result (Result(..))
import Data.Tuple (Tuple(..))
import Data.Tuple as Tuple
import Rook.Syntax.Common (Mutability(..), PrimOp(..), PrimType(..), Reference, RookType(..), DefParam)
import Rook.Syntax.Literal (Literal(..))
import Rook.Syntax.Path (Path)
import Rook.Syntax.Pattern (Pattern(..))
import Rook.Syntax.Pattern as Pattern
import Rook.Types.Unification (Substitution, TypeConstraint(..))


class Printable a where
  print :: a -> String

instance Printable (Tuple Path String) where
  print (Tuple path str) = intercalate "." path <> "." <> str

instance Printable (Expr a) where
  print = printExpr Nil

instance Printable (Stmt a) where
  print = printStmt Nil >>> Tuple.snd

printExpr :: List String -> Expr _ -> String
printExpr xs e =
  case e of
    Global _ path name ->
      print (Tuple path name)

    Local _ i ->
      case List.index xs i of
        Just x ->
          x

        Nothing ->
          "<" <> show i <> ">"

    PropertyAccess expr _ prop ->
      printExpr xs expr <> "." <> prop

    Ctor _ path ->
      intercalate "." path

    BlockExpr _ stmts final ->
      let
        Tuple xs' str = printStmts xs stmts
        finalStr = fromMaybe "" ((\x -> " " <> x) <<< printExpr xs' <$> final)
      in "{ " <> str <> finalStr <> " }"

    If _ guard thenExpr maybeElseExpr ->
      "if (" <> printExpr xs guard <> ") { " <> printExpr xs thenExpr <> " }"
        <> fromMaybe "" (map (\e' -> " { " <> printExpr xs e' <> " }") maybeElseExpr)

    FunExpr _ params typeAnn body ->
      let
        paramStr = intercalate ", " $ map printParam params
        printParam { pattern : p, annotation : t } = printPattern p <> printTypeAnn t
        typeStr = printTypeAnn typeAnn
        xs' = foldl collect xs $ map (\x -> x.pattern) params
        collect ys p = Pattern.bindings p <> ys
      in "(" <> paramStr <> ") " <> typeStr <> "=> " <> printExpr xs' body

    Match _ e' arms ->
      let
        armsStr = intercalate ", " $ map (printArm xs) arms
      in
        "match " <> printExpr xs e' <> " { " <> armsStr <> " }"

    LitExpr _ lit ->
      printLit lit

    Call func _ args ->
      let
        funcStr = printExpr xs func
        fStr = if isAtomicExpr func then inParens funcStr else funcStr
      in
        fStr <> "(" <> intercalate ", " (map (printArg xs) args) <> ")"

    PrimOpExpr _ op ->
      printOp op

    RefExpr _ ref ->
      printRef ref

printRef :: Reference -> String
printRef { frame, index } = "@{" <> show frame <> ":" <> show index <> "}"

printOp :: PrimOp -> String
printOp = case _ of
  Print -> "print"

printArg :: List String -> CallArg _ -> String
printArg xs maybeE =
  case maybeE of
    ExprArg e ->
      printExpr xs e

    PlaceholderArg _ ->
      "%_"

isAtomicExpr :: Expr _ -> Boolean
isAtomicExpr e =
  case e of
    Global _ _ _ ->
      true

    Local _ _ ->
      true

    LitExpr _ _ ->
      true

    _ ->
      false

inParens :: String -> String
inParens str = "(" <> str <> ")"

printArm :: List String -> MatchArm _ -> String
printArm xs { definition : arm , pattern } =
  printPattern pattern <> " => " <> printExpr xs arm

printStmts :: List String -> List (Stmt _) -> Tuple (List String) String
printStmts xs stmts =
  foldl collect (Tuple xs Nil) stmts
    # map (intercalate " " <<< List.reverse)
  where
    collect :: Tuple (List String) (List String) -> Stmt _ -> Tuple (List String) (List String)
    collect (Tuple ys acc) s = map (\x -> x : acc) $ printStmt ys s

printStmt :: List String -> Stmt _ -> Tuple (List String) String
printStmt xs s =
  case s of
    DeclStmt _ dt x t e ->
      let
        Tuple dtStr opStr =
          case dt of
            Let -> Tuple "let " " := "
            Mut -> Tuple "mut " " <- "

        tStr = printTypeAnn t

        str = dtStr <> x <> opStr <> tStr <> printExpr xs e

      in
        Tuple (x : xs) str

    AssignGlobal _ path name e ->
      let
        str = print (Tuple path name) <> " <- " <> printExpr xs e
      in
        Tuple xs str

    AssignLocal _ i e ->
      let
        x = fromMaybe ("<" <> show i <> ">") $ List.index xs i
        str = x <> " <- " <> printExpr xs e
      in
        Tuple xs str

    AssignRef _ r e ->
      Tuple xs $ printRef r

    For _ pat e stmts ->
      let
        patVars = Pattern.bindings pat
        Tuple _ blockStr = printStmts (patVars <> xs) stmts
        str = "for ("
          <> printPattern pat
          <> " of "
          <> printExpr xs e
          <> ") { "
          <> blockStr
          <> " }"
      in
        Tuple xs str

    While _ e stmts ->
      let
        Tuple _ blockStr = printStmts xs stmts
        str = "while (" <> printExpr xs e <> ") { " <> blockStr <> " }"
      in
        Tuple xs str

    Loop _ stmts ->
      let
        Tuple _ blockStr = printStmts xs stmts
        str = "loop { " <> blockStr <> " }"
      in
        Tuple xs str

    Break _ ->
      Tuple xs "break;"

    Continue _ ->
      Tuple xs "continue;"

    ExprStmt e ->
      let
        str = printExpr xs e <> terminator e
        terminator e' = case e' of
          BlockExpr _ _ _ -> ""
          If _ _ _ _ -> ""
          FunExpr _ _ _ e'' -> (defer \_ -> terminator) e''
          Match _ _ _ -> ""
          _ -> ";"
      in
        Tuple xs str

printType :: RookType _ -> String
printType t =
  case t of
    Forall _ ts u ->
      "[" <> intercalate "," ts <> "] " <> printType u

    -- Variant _ name _ ->
    --   "<<VARIANT " <> name <> " >>"

    Arrow _ argTypes returnType ->
      "(" <> intercalate ", " (printType <$> argTypes) <> ") -> "
        <> printType returnType

    Primitive _ p ->
      printPrimType p

    TypeVar _ x ->
      x

    TypeApp _ u ts ->
      let
        uStr = printType u
        uStr' = if isAtomicType u then inParens uStr else uStr
      in
        uStr' <> "<" <> intercalate "," (printType <$> ts) <> ">"

    TypeAlias _ path ->
      intercalate "." path

printPrimType :: PrimType -> String
printPrimType p =
  case p of
    IntType -> "Int"
    StringType -> "String"
    BoolType -> "Bool"
    UnitType -> "Unit"
    NeverType -> "Never"

isAtomicType :: RookType _ -> Boolean
isAtomicType t =
  case t of
    TypeVar _ _ ->
      true

    Primitive _ _ ->
      true

    TypeAlias _ _ ->
      true

    _ ->
      false

printTypeAnn :: Maybe (RookType _) -> String
printTypeAnn typeAnn =
  case typeAnn of
    Just t ->
      ": " <> printType t <> " "

    Nothing ->
      ""

printPattern :: Pattern _ -> String
printPattern p =
  case p of
    PatternVar _ x ->
      x

    CtorPattern _ path pats ->
      intercalate "." path <> "(" <> intercalate ", " (map printPattern pats) <> ")"

    LiteralPattern _ lit ->
      printLit lit

    WildcardPattern _ ->
      "_"

printPath :: Path -> String -> String
printPath path name = intercalate "." (path <> pure name)

printLit :: Literal -> String
printLit lit =
  case lit of
    IntLit n ->
      show n

    StringLit s ->
      show s

    BoolLit b ->
      if b then "true" else "false"

    UnitLit ->
      "()"

instance Printable (RookType a) where
  print = printType

instance Printable a => Printable (Result e a) where
  print r = case r of
    Ok x -> print x
    Error _ -> "<<ERROR>>"

printSubs :: Substitution -> String
printSubs s =
  "[ " <> intercalate ", " (foldrWithIndex printSub Nil s) <> " ]"
    where
      printSub :: String -> RookType _ -> List String -> List String
      printSub x t acc = (x <> " ↦ " <> printType t) : acc

printConstraint :: TypeConstraint -> String
printConstraint (TypeConstraint t1 t2) = printType t1 <> " = " <> printType t2

printConstraints :: forall f. Foldable f => Functor f => f TypeConstraint -> String
printConstraints cs =
  "{ " <> (intercalate ", " $ map printConstraint cs) <> " }"

-- printValue :: Value -> String
-- printValue val =
--   case val of
--     LitValue lit ->
--       printLit lit

--     FunValue xs e ->
--       "(" <> intercalate "," xs <> ") => " <> printExpr xs e

printDeclaration :: Declaration _ -> String
printDeclaration = case _ of
  Def _ typeVars name params t body ->
    let
      typeVarsStr =
        if typeVars == Nil then
          ""
        else
          "[" <> intercalate "," typeVars <> "] "
      {paramsStr, bindings} =
        case params of
          Just ps ->
            { paramsStr: "(" <> intercalate "," (printDefParam <$> ps) <> ")"
            , bindings: ps <#> \{name} -> name
            }
          Nothing ->
            { paramsStr: ""
            , bindings: Nil
            }
    in
      "def "
        <> typeVarsStr
        <> name
        <> paramsStr
        <> printTypeAnn (Just t)
        <> " := "
        <> printExpr bindings body

  Mod _ name _ ->
    "<<module " <> name <> ">>"

printDefParam :: DefParam _ -> String
printDefParam {name, annotation: t} = name <> printTypeAnn (Just t)