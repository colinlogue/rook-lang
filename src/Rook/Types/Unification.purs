module Rook.Types.Unification where

import Prelude
import Rook.Syntax.Common

import Data.Bifunctor (bimap)
import Data.Foldable (elem, fold, foldl, foldr)
import Data.Generic.Rep (class Generic)
import Data.List (List(..), (:))
import Data.List as List
import Data.Map (Map)
import Data.Map as Map
import Data.Maybe (Maybe(..))
import Data.Result (Result(..))
import Data.Set (Set)
import Data.Set as Set
import Data.Show.Generic (genericShow)
import Data.Tuple (Tuple(..))
import Data.Tuple as Tuple
import Debug as Debug


data TypeConstraint = TypeConstraint (RookType Unit) (RookType Unit)

derive instance Eq TypeConstraint
derive instance Generic TypeConstraint _
instance Show TypeConstraint where
  show x = genericShow x

type Substitution = Map String (RookType Unit)

isFreeIn :: RookType _ -> String -> Boolean
isFreeIn t x = Set.member x $ freeVars t

freeVars :: RookType _ -> Set String
freeVars t =
  case t of
    Forall _ xs t' ->
      foldr Set.delete (freeVars t') xs

    -- Variant _ x ctorDefs ->
    --   Set.delete x
    --     $ Set.unions
    --     $ map (Tuple.snd >>> map freeVars >>> Set.unions) ctorDefs

    Arrow _ paramTypes returnType ->
      Set.union (Set.unions $ map freeVars paramTypes) $ freeVars returnType

    Primitive _ _ ->
      Set.empty

    TypeVar _ x ->
      Set.singleton x

    TypeApp _ t' ts ->
      Set.union (freeVars t') $ Set.unions $ map freeVars ts

    TypeAlias _ _ ->
      Set.empty

composeSubs :: Substitution -> Substitution -> Substitution
composeSubs sigma gamma = Map.union (map (subAll sigma) gamma) sigma

unify :: List TypeConstraint -> Maybe Substitution
unify ts =
  case ts of
    Nil ->
      pure Map.empty

    Cons (TypeConstraint s t) rest
      | s == t -> unify rest
      | otherwise ->
        case Tuple s t of
          Tuple (TypeVar _ x) _ | not (isFreeIn t x) ->
            do
              sigma <- unify (map (subConstraint x t) rest)
              let gamma = Map.singleton x t
              pure $ composeSubs sigma gamma

          Tuple _ (TypeVar _ x) | not (isFreeIn s x) ->
            do
              sigma <- unify (map (subConstraint x s) rest)
              let gamma = Map.singleton x s
              pure $ composeSubs sigma gamma

          Tuple (Arrow _ sParams sReturn) (Arrow _ tParams tReturn)
          | List.length sParams == List.length tParams ->
              let
                cs = foldl
                  (\acc (Tuple a b) -> TypeConstraint a b : acc)
                  rest
                  (List.zip sParams tParams)
              in
                unify (TypeConstraint sReturn tReturn : cs)

          -- ignore the foralls?
          Tuple (Forall _ _ s') _ ->
            unify (TypeConstraint s' t : rest)

          Tuple _ (Forall _ _ t') ->
            unify (TypeConstraint s t' : rest)

          -- TODO: For now at least, variants must be identical.
          -- Tuple (Variant _ x sCtors) (Variant _ y tCtors)
          --   | x == y && List.length sCtors == List.length tCtors ->
          --       Debug.todo "to do: unify variants"

          --   | x /= y ->
          --       Error $ VariantMismatchName x y

          --   | otherwise ->
          --       Error $ VariantMismatchNumCtors x

          Tuple (TypeApp _ s' ss) (TypeApp _ t' ts)
          | List.length ss == List.length ts ->
              let
                cs = foldl
                  (\acc (Tuple a b) -> TypeConstraint a b : acc)
                  rest
                  (List.zip ss ts)
              in
                unify (TypeConstraint s' t' : cs)

          _ ->
            Nothing

sub :: String -> RookType _ -> RookType _ -> RookType Unit
sub x s t =
  case t of
    Forall _ xs u
      | elem x xs -> unchanged
      | otherwise -> Forall unit xs $ sub x s u

    -- Variant _ y ctorDefs ->
    --   Variant unit y $ map (map $ map $ sub x s) ctorDefs

    Arrow _ paramTypes returnType ->
      Arrow unit (map (sub x s) paramTypes) (sub x s returnType)

    Primitive _ _ ->
      unchanged

    TypeVar _ y
      | y == x -> s
      | otherwise -> unchanged

    TypeApp _ u ts ->
      TypeApp unit (sub x s u) (map (sub x s) ts)

    TypeAlias _ _ ->
      unchanged

  where
    unchanged = void t

subConstraint :: String -> RookType _ -> TypeConstraint -> TypeConstraint
subConstraint x s (TypeConstraint t u) =
  let sigma = Map.singleton x s
  in TypeConstraint (subAll sigma t) (subAll sigma u)

fresh :: String -> RookType _ -> String
fresh x t =
  if isFreeIn t x
  then fresh (x <> "!") t
  else x

subAll :: Substitution -> RookType _ -> RookType Unit
subAll s t =
  case t of
    Forall _ xs u ->
      let s' = foldr (Map.delete) s xs
      in Forall unit xs $ subAll s' u

    -- Variant _ y ctorDefs ->
    --   Variant unit y $ map (map $ map $ subAll s) ctorDefs

    Arrow _ paramTypes returnType ->
      Arrow unit (map (subAll s) paramTypes) (subAll s returnType)

    Primitive _ _ ->
      unchanged

    TypeVar _ y ->
      case Map.lookup y s of
        Just u ->
          u

        Nothing ->
          unchanged

    TypeApp _ u ts ->
      TypeApp unit (subAll s u) (map (subAll s) ts)

    TypeAlias _ _ ->
      unchanged

  where
    unchanged = void t