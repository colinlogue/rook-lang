module Rook.Parse.Token where

import Prelude

import Control.Alt ((<|>))
import Control.Lazy (defer)
import Data.Either (choose)
import Data.Foldable (foldl, oneOf)
import Data.List (List(..), many)
import Data.List as List
import Data.List.Types as NonEmpty
import Data.Maybe (Maybe(..), optional)
import Data.Tuple (Tuple(..))
import Data.Tuple as Tuple
import Text.Parsing.Parser (Parser, fail)
import Text.Parsing.Parser.Combinators (lookAhead, many1, option, try, (<?>))
import Text.Parsing.Parser.String (char)
import Text.Parsing.Parser.Token (GenLanguageDef(..), LanguageDef, TokenParser, alphaNum, letter, makeTokenParser, upper)
import Text.Parsing.Parser.Token.Extra (lower)

identStart ∷ Parser String Char
identStart = letter

identLetter :: Parser String Char
identLetter = alphaNum <|> char '_'

reservedNames :: Array String
reservedNames =
    [ "for"
    , "of"
    , "while"
    , "loop"
    , "if"
    , "else"
    , "def"
    , "match"
    , "with"
    , "break"
    , "continue"
    , "let"
    , "mut"
    , "true"
    , "false"
    ]

rookDef :: LanguageDef
rookDef = LanguageDef
  { commentStart : "/*"
  , commentEnd : "*/"
  , commentLine : "//"
  , nestedComments : true
  , identStart : letter
  , identLetter : alphaNum <|> char '_'
  , opStart : oneOf []
  , opLetter : oneOf []
  , reservedOpNames : []
  , reservedNames : reservedNames
  , caseSensitive : true
  }

rookLexer :: TokenParser
rookLexer = makeTokenParser rookDef

type RookParser a = Parser String a

ident :: RookParser String
ident = rookLexer.identifier

reserved :: String -> RookParser Unit
reserved = rookLexer.reserved

stringLit :: RookParser String
stringLit = rookLexer.stringLiteral

intLit :: RookParser Int
intLit = rookLexer.integer

symbol :: String -> RookParser String
symbol = rookLexer.symbol

whitespace :: RookParser Unit
whitespace = rookLexer.whiteSpace

parens :: forall a. RookParser a -> RookParser a
parens = rookLexer.parens

braces :: forall a. RookParser a -> RookParser a
braces = rookLexer.braces

angles :: forall a. RookParser a -> RookParser a
angles = rookLexer.angles

brackets :: forall a. RookParser a -> RookParser a
brackets = rookLexer.brackets

semi :: RookParser String
semi = rookLexer.semi

dot :: RookParser String
dot = rookLexer.dot

semiSep :: forall a. RookParser a -> RookParser (List a)
semiSep = rookLexer.semiSep

commaSep :: forall a. RookParser a -> RookParser (List a)
commaSep = rookLexer.commaSep

smallArrow :: RookParser Unit
smallArrow = void $ symbol "->"

bigArrow :: RookParser Unit
bigArrow = void $ symbol "=>"

colon :: RookParser Unit
colon = void $ symbol ":"

lowerIdent :: RookParser String
lowerIdent = lookAhead lower *> ident

upperIdent :: RookParser String
upperIdent = lookAhead upper *> ident

underscore :: RookParser Unit
underscore = void $ symbol "_"

placeholder :: RookParser Unit
placeholder = void $ symbol "%_"

equals :: RookParser Unit
equals = void $ symbol ":="

assignmentOp :: RookParser Unit
assignmentOp = void $ symbol "<-"

lexeme :: forall a. RookParser a -> RookParser a
lexeme = rookLexer.lexeme

lparen :: RookParser Unit
lparen = void $ symbol "("

rparen :: RookParser Unit
rparen = void $ symbol ")"

plus :: RookParser Unit
plus = void $ symbol "+"

minus :: RookParser Unit
minus = void $ symbol "-"

times :: RookParser Unit
times = void $ symbol "*"

divide :: RookParser Unit
divide = void $ symbol "/"

modulus :: RookParser Unit
modulus = void $ symbol "%"

