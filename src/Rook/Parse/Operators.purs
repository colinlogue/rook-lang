module Rook.Parse.Operators where

import Prelude

import Data.Either (Either(..))

-- import Data.Array as Array
-- import Data.Either (Either)
-- import Data.Maybe (Maybe)



-- data Dir = Left | Right

-- data LeadOrTrail = Leading | Trailing | LeadingAndTrailing

-- type OperatorDef sym term =
--   { symbols :: Array sym
--   , leadOrTrail :: LeadOrTrail
--   , applyOp :: Array term -> Maybe term
--   }

-- plusOpDef :: OperatorDef
-- plusOpDef = binaryOpDef "+"

-- binaryOpDef :: String -> OperatorDef
-- binaryOpDef sym = {symbols: [sym], leadOrTrail: LeadingAndTrailing}

-- data CollectError = CollectError

-- type ExprDef op sym =
--   { bindStrength :: op -> Int
--   , opDefs :: op -> OperatorDef sym
--   , getDir :: op -> op -> Maybe Dir
--   }

-- type OpFrame term sym =
--   { preSym :: Maybe sym
--   , collected :: List (Tuple term sym)
--   }

-- check :: OpFrame term sym -> OperatorDef sym term -> Maybe (Either (OpFrame term sym) term)
-- check {preSym, collected} {symbols, leadOrTrail, applyOp} =
--   do
--     remSyms <- case preSym of
--       Nothing ->
--         case leadOrTrail of
--           Leading -> Nothing
--           LeadingAndTrailing -> Nothing
--           Trailing -> Array.tail symbols
--       Just s ->
--         case leadOrTrail of
--           Leading | LeadingAndTrailing ->
--             Nothing

--           Trailing ->
--             Nothing
--     Nothing

ex :: Either Int Int -> Int
ex = case _ of
  Left x | Right x -> x

-- collectExpr :: forall term op sym f
--   . Foldable f
--   => ExprDef op sym
--   -> Maybe sym
--   -> term
--   -> f (Tuple sym term)
--   -> Maybe sym
--   -> Result CollectError term
-- collectExpr exprDef preSym t0 stack postSym =
--   do
--     let {bindStrength, opDefs, getDir} = exprDef

