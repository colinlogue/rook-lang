module Rook.Syntax where

-- import Prelude

-- import Control.Lazy (defer)
-- import Data.Foldable (foldMap)
-- import Data.Generic.Rep (class Generic)
-- import Data.List (List(..))
-- import Data.Maybe (Maybe)
-- import Data.Set (Set)
-- import Data.Show.Generic (genericShow)
-- import Data.Tuple (Tuple)
-- import Test.QuickCheck.Arbitrary (class Arbitrary, genericArbitrary)

-- -- TODO: allow pattern matching on primitive types
-- data Pattern a
--   = PatternVar a String
--   | CtorPattern a Path String (List (Pattern a))
--   | LiteralPattern a Literal
--   | WildcardPattern a

-- derive instance eqPattern :: Eq a => Eq (Pattern a)
-- derive instance genericPattern :: Generic (Pattern a) _
-- derive instance functorPattern :: Functor Pattern

-- instance showPattern :: Show a => Show (Pattern a) where
--   show x = genericShow x

-- data Literal
--   = IntLit Int
--   | StringLit String
--   | BoolLit Boolean
--   | UnitLit

-- derive instance eqLiteral :: Eq Literal
-- derive instance genericLiteral:: Generic Literal _

-- instance showLiteral :: Show Literal where
--   show x = genericShow x

-- type Path = List String
-- type ModulePath =
--   { path :: Path
--   , name :: String
--   }

-- -- derive newtype instance eqModulePath :: Eq Path
-- -- derive newtype instance showModulePath :: Show Path

-- type MatchArm a = { pattern :: Pattern a, arm :: Expr a }

-- type FunParam a = { pattern :: Pattern a, type :: Maybe (RookType a) }

-- data Expr a
--   = Global a Path String
--   | Local a Int
--   | Ctor a Path String
--   -- | FieldAccess a (Expr a) (List String)
--   | Block a (List (Stmt a)) (Expr a)
--   | If a (Expr a) (Expr a) (Maybe (Expr a))
--   | FunExpr a (List (FunParam a)) (Maybe (RookType a)) (Expr a)
--   | Match a (Expr a) (List (MatchArm a))
--   | Lit a Literal
--   | Call a (Expr a) (List (Maybe (Expr a)))

-- derive instance eqExpr :: Eq a => Eq (Expr a)
-- derive instance functorExpr :: Functor Expr
-- derive instance genericExpr :: Generic (Expr a) _

-- instance showExpr :: Show a => Show (Expr a) where
--   show x = genericShow x

-- data DeclType
--   = Let
--   | Mut

-- derive instance eqDeclType:: Eq DeclType
-- derive instance genericDeclType:: Generic DeclType _

-- instance showDeclType:: Show DeclType where
--   show x = genericShow x

-- data Control
--   = Break
--   | Continue

-- derive instance Eq Control
-- derive instance Generic Control _

-- instance Show Control where
--   show x = genericShow x

-- data Stmt a
--   = DeclStmt a DeclType String (Maybe (RookType a)) (Expr a)
--   | AssignGlobal a Path String (Expr a)
--   | AssignLocal a Int (Expr a)
--   | For a (Pattern a) (Expr a) (List (Stmt a))
--   | While a (Expr a) (List (Stmt a))
--   | Loop a (List (Stmt a))
--   | ControlStmt a Control
--   | ExprStmt (Expr a)

-- derive instance eqStmt :: Eq a => Eq (Stmt a)
-- derive instance functorStmt :: Functor Stmt
-- derive instance genericStmt :: Generic (Stmt a) _

-- instance showStmt :: Show a => Show (Stmt a) where
--   show x = genericShow x

-- data RookType a
--   = Forall a (List String) (RookType a)
--   | Variant a String (List (Tuple String (List (RookType a))))
--   | Arrow a (List (RookType a)) (RookType a)
--   | Primitive a PrimType
--   | TypeVar a String
--   | TypeApp a (RookType a) (List (RookType a))
--   | TypeAlias a Path String

-- derive instance eqRookType :: Eq a => Eq (RookType a)
-- derive instance functorType :: Functor RookType
-- derive instance genericRookType :: Generic (RookType a) _

-- instance showRookType:: Show a => Show (RookType a) where
--   show x = genericShow x

-- data PrimType
--   = IntType
--   | StringType
--   | BoolType
--   | UnitType
--   | NeverType

-- derive instance eqPrimType :: Eq PrimType
-- derive instance genericPrimType :: Generic PrimType _

-- instance showPrimType:: Show PrimType where
--   show x = genericShow x

-- data Kind
--   = Star
--   | StarArrow Kind

-- data Value
--   = LitValue Literal
--   | FunValue (List String) (Expr Unit)

-- type Binding = { name :: String, value :: Value }

-- derive instance Eq Value
-- derive instance Generic Value _

-- instance Show Value where
--   show x = genericShow x

-- type Import a =
--   { qualified :: Maybe String
--   , path :: ModulePath
--   , names :: Set String
--   , info :: a
--   }

-- type Export a =
--   { name :: String
--   , info :: a
--   }

-- data Declaration a
--   = Def a String (RookType a) (Expr a)
--   | Mod a (Module a)

-- type Module a =
--   { path :: ModulePath
--   , imports :: Set (Import a)
--   , exports :: Set (Export a)
--   , declarations :: List (Declaration a)
--   }

-- -- type ImportItem = String

-- -- data Import
-- --   = UnqualifiedImport Path
-- --   | QualifiedImport String Path (List I)

-- -- data Program = Program
-- --   { path :: Path
-- --   , imports :: List Import
-- --   , declarations :: List TopLevel
-- --   , main :: Maybe
-- --   }

-- patternBindings :: forall a. Pattern a -> List String
-- patternBindings pat =
--   case pat of
--     PatternVar _ x ->
--       pure x

--     CtorPattern _ _ _ pats ->
--       foldMap patternBindings pats

--     WildcardPattern _ ->
--       Nil

--     LiteralPattern _ _ ->
--       Nil

-- paramBindings :: forall a. FunParam a -> List String
-- paramBindings { pattern } = patternBindings pattern

-- -- instance Arbitrary (Expr Unit) where
-- --   arbitrary = defer \_ -> genericArbitrary

-- -- instance Arbitrary (Stmt Unit) where
-- --   arbitrary = defer \_ -> genericArbitrary

-- -- instance Arbitrary DeclType where
-- --   arbitrary = genericArbitrary

-- -- instance Arbitrary (RookType Unit) where
-- --   arbitrary = genericArbitrary

-- -- instance Arbitrary (Pattern Unit) where
-- --   arbitrary = defer \_ -> genericArbitrary

-- -- instance Arbitrary Control where
-- --   arbitrary = genericArbitrary

-- -- instance Arbitrary PrimType where
-- --   arbitrary = genericArbitrary

-- -- instance Arbitrary Literal where
-- --   arbitrary = genericArbitrary