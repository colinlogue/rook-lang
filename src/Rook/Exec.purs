module Rook.Exec where

-- import Prelude
-- import Rook.Syntax

-- import Control.Monad.Comp (Comp)
-- import Control.Monad.Error.Class (throwError)
-- import Control.Monad.State as State
-- import Data.Either (Either(..))
-- import Data.Either.Extra as Either
-- import Data.Foldable (foldl)
-- import Data.List (List(..))
-- import Data.List as List
-- import Data.Maybe (Maybe(..))
-- import Data.Result (Result(..))
-- import Data.Stack (Stack)
-- import Data.Traversable (traverse, traverse_)
-- import Data.Tuple (Tuple(..))
-- import Debug as Debug
-- import Partial.Unsafe (unsafeCrashWith)
-- import Rook.Context (Context)
-- import Rook.Context as Context
-- import Rook.Print (printPath, printValue)

-- data Reference = Ref Int

-- type ExecContext =
--   { context :: Context Value
--   , heap :: Array (Maybe Value)
--   }

-- type ExecResult a = Result ExecError a

-- type ExecError =
--   { stack :: Stack { name :: String, val :: Value }
--   , msg :: String
--   }

-- type Exec m a = Comp m ExecContext ExecError a

-- class Monad m <= Interp m where
--   read :: m String
--   write :: String -> m Unit

-- execCrash :: forall m a. Interp m => String -> Exec m a
-- execCrash msg =
--   do
--     { locals } <- context
--     throwError { stack : locals, msg }

-- context :: forall m. Interp m => Exec m ExecContext
-- context = State.get

-- lookupGlobal :: forall m. Interp m => Path -> String -> Exec m Value
-- lookupGlobal path name =
--   do
--     cxt <- context
--     case Context.lookup (Tuple path name) cxt of
--       Just val ->
--         pure val

--       Nothing ->
--         execCrash $ "unknown global: " <> printPath path name

-- lookupLocal :: forall m. Interp m => Int -> Exec m Value
-- lookupLocal i =
--   do
--     cxt <- context
--     case Context.lookup i cxt of
--       Just val ->
--         pure val

--       Nothing ->
--         execCrash $ "local out of bounds: " <> show i

-- scoped :: forall m a. Interp m => Exec m a -> Exec m a
-- scoped ex =
--   do
--     state <- State.get
--     result <- ex
--     State.put state
--     pure result

-- eval :: forall m. Interp m => Expr _ -> Exec m Value
-- eval expr =
--   case expr of
--     Global _ path name ->
--       lookupGlobal path name

--     Local _ i ->
--       lookupLocal i

--     Ctor _ _ _ ->
--       unsafeCrashWith "to do: eval constructor"

--     Block _ stmts final ->
--       scoped do
--         traverse_ exec stmts
--         eval final

--     If _ guard thenExpr maybeElseExpr ->
--       do
--         guardVal <- eval guard
--         case guardVal of
--           LitValue (BoolLit b) ->
--             if b
--             then eval thenExpr
--             else case maybeElseExpr of
--               Just elseExpr -> eval elseExpr
--               Nothing -> pure $ LitValue UnitLit
--           _ ->
--             execCrash $ "non-boolean if guard: " <> printValue guardVal

--     FunExpr _ params _ body ->
--       let
--         collectBindings :: List String -> FunParam _ -> List String
--         collectBindings acc { pattern } = acc <> patternBindings pattern

--         bindings :: List String
--         bindings = foldl collectBindings Nil params
--       in
--         pure $ FunValue bindings Nil body

--     Match _ scrutinee arms ->
--       do
--         v <- eval scrutinee
--         unsafeCrashWith "to do: eval match"

--     Lit _ lit ->
--       pure $ LitValue lit

--     Call _ funcExpr argExprs ->
--       do
--         f <- eval funcExpr
--         { params, bindings, body } <- asFunValue f
--         Tuple remainingParams newBindings <- Either.separate
--           <$> (traverse evalArg $ List.zip params argExprs)
--         let boundVals = bindings <> newBindings
--         case remainingParams of
--           Nil ->
--             do
--               traverse_ (\{ name, value } -> extend name value) boundVals
--               eval body

--           _ ->
--             pure $ FunValue remainingParams boundVals body

--       where
--         asFunValue
--           :: Value
--           -> Exec m
--             { params :: List String
--             , bindings :: List Binding
--             , body :: Expr Unit }
--         asFunValue val =
--           case val of
--             FunValue params bindings body ->
--               pure { params, bindings, body }

--             _ ->
--               execCrash $ "not a function value: " <> printValue val

--         evalArg :: Tuple String (Maybe (Expr _)) -> Exec m (Either String Binding)
--         evalArg (Tuple name maybeArg) =
--           case maybeArg of
--             Just arg ->
--               do
--                 value <- eval arg
--                 pure $ Right $ { name, value }

--             Nothing ->
--               pure $ Left name

-- -- The boolean indicates if we should break out of the current loop.
-- exec :: forall m. Interp m => Stmt _ -> Exec m (Maybe Control)
-- exec stmt =
--   case stmt of
--     DeclStmt _ _ name _ expr ->
--       do
--         v <- eval expr
--         extend name v
--         pure Nothing

--     AssignGlobal _ path name expr ->
--       do
--         v <- eval expr
--         assignGlobal path name v
--         pure Nothing

--     For _ _ _ _ ->
--       unsafeCrashWith "to do: exec for"

--     While _ expr stmts ->
--       -- TODO: Loop limit is temporary during development.
--       -- let loopLimit = 1000 in
--       do
--         b <- eval expr
--         case b of
--           LitValue (BoolLit true) ->
--             do
--               control <- scoped $ execBlock stmts
--               case control of
--                 Just Break ->
--                   pure Nothing

--                 _ ->
--                   exec $ While unit expr stmts

--           LitValue (BoolLit false) ->
--             pure Nothing

--           _ ->
--             execCrash $ "while guard is not a boolean: " <> printValue b

--     Loop _ stmts ->
--       do
--         control <- scoped $ execBlock stmts
--         case control of
--           Just Break ->
--             pure Nothing

--           _ ->
--             exec $ Loop unit stmts

--     ControlStmt _ control ->
--       pure $ Just control

--     _ ->
--       unsafeCrashWith "to do: exec"

-- execBlock :: forall m. Interp m => List (Stmt _) -> Exec m (Maybe Control)
-- execBlock stmts =
--   case stmts of
--     Nil ->
--       pure Nothing

--     Cons stmt rest ->
--       do
--         control <- exec stmt
--         case control of
--           Just _ ->
--             pure control

--           Nothing ->
--             execBlock rest

-- extend :: forall m. Interp m => String -> Value -> Exec m Unit
-- extend name val =
--   do
--     cxt <- context
--     case Context.extend name val cxt of
--       Ok cxt' ->
--         State.put cxt'

--       Error _ ->
--         execCrash $ "name already bound: " <> name

-- assignGlobal :: forall m. Interp m => Path -> String -> Value -> Exec m Unit
-- assignGlobal path name val =
--   do
--     cxt <- context
--     case Context.updateGlobal path name val cxt of
--       Ok cxt' ->
--         State.put cxt'

--       _ ->
--         execCrash $ "failed to update global: " <> printPath path name

-- assignLocal :: forall m. Interp m => Int -> Value -> Exec m Unit
-- assignLocal i val =
--   do
--     cxt <- context
--     case Context.updateLocal i val cxt of
--       Ok cxt' ->
--         State.put cxt'

--       _ ->
--         execCrash $ "failed to update local: " <> show i


