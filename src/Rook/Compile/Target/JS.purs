module Rook.Compile.Target.JS where

import Prelude
import Rook.Syntax

import Control.Monad.State (runState)
import Control.Monad.State as State
import Control.Monad.State.Trans (StateT)
import Control.Monad.Writer (WriterT, runWriterT)
import Control.Monad.Writer as Writer
import Control.Parallel (sequential)
import Data.Counter (Counter, runCounter)
import Data.Counter as Counter
import Data.Foldable (indexl, intercalate, traverse_)
import Data.Identity (Identity)
import Data.List (List(..), (:))
import Data.List as List
import Data.Maybe (Maybe(..))
import Data.Result (Result(..))
import Data.Traversable (for, sequence)
import Data.Tuple as Tuple
import Effect (Effect)
import Effect.Aff (launchAff_)
import Effect.Class.Console as Console
import Partial.Unsafe (unsafeCrashWith)
import Rook.Binding (captureModuleVars)
import Rook.Pass.Full (parseFile)
import Rook.Print (printDeclaration)
import Rook.Syntax.Bound (CallArg(..), Declaration(..), Expr(..), Module, Stmt(..), MatchArm)
import Rook.Syntax.Common (DefParam, Mutability(..))
import Rook.Syntax.Literal (Literal(..))
import Rook.Syntax.Path (Path)
import Rook.Syntax.Pattern (Pattern(..))


type EmitContext =
  { locals :: List String
  , indentLevel :: Int
  }

type Emitter = WriterT String (StateT EmitContext Identity)

getLocal :: Int -> Emitter String
getLocal i = do
  { locals } <- State.get
  pure case indexl i locals of
    Just x ->
      x <> show i
    Nothing ->
      unsafeCrashWith "local out of bounds"

scoped :: forall a. Emitter a -> Emitter a
scoped em = do
  {locals} <- State.get
  val <- em
  State.modify_ \s -> s {locals = locals}
  pure val

addLocal :: String -> Emitter Unit
addLocal x = State.modify_ \s -> s { locals = x : s.locals }

emit :: String -> Emitter Unit
emit = Writer.tell

emitLine :: String -> Emitter Unit
emitLine str = emit (str <> "\n")

emitIndented :: String -> Emitter Unit
emitIndented str = getIndent >>= \i -> emit (indented i str)

emitIndentedLine :: String -> Emitter Unit
emitIndentedLine str = getIndent >>= \i -> emitLine (indented i str)

emitIndent :: Emitter Unit
emitIndent = emitIndented ""

getIndent :: Emitter Int
getIndent = State.gets \s -> s.indentLevel

indent :: Emitter Unit
indent = State.modify_ (\s -> s { indentLevel = s.indentLevel + 1 })

deindent :: Emitter Unit
deindent = State.modify_ (\s -> s { indentLevel = max 0 (s.indentLevel - 1)})

indentStr :: String
indentStr = "    "

indented :: Int -> String -> String
indented i str =
  if i > 0 then
    indented (i - 1) (indentStr <> str)
  else
    str

inlineExpr :: Expr _ -> Emitter Unit
inlineExpr = case _ of
  Global _ path name ->
    emit $ intercalate "." $ List.snoc path name

  Local _ i ->
    getLocal i >>= emit

  RefExpr _ _ ->
    unsafeCrashWith "refs???"

  Ctor _ path ->
    emit $ intercalate "." path

  PropertyAccess e _ prop ->
    inlineExpr e <> emit prop

  FunExpr _ params _ body -> scoped do
    emit "(("
    intercalate (emit ", ") ((\{pattern} -> emitPattern pattern) <$> params)
    emit ") => "
    case body of
      BlockExpr _ stmts final -> emitFunBody stmts final <> emit "...."
      _ -> inlineExpr body
    emit ")"

  Call e _ args -> do
    inlineExpr e
    emit "("
    intercalate (emit ", ") (emitCallArg <$> args)
    emit ")"

  LitExpr _ lit ->
    emit $ showLit lit

  BlockExpr _ stmts final ->
    do
      emitLine "(() => "
      emitFunBody stmts final
      emit ")()"

  Match _ e arms -> do
    emit "$match("
    inlineExpr e
    emitLine ","
    indent
    intercalate (emit ",\n") ((\a -> emitIndent <> emitMatchArm a) <$> arms)
    emitLine ""
    deindent
    emitIndented "})"

  If _ e1 e2 e3 -> do
    emit "("
    inlineExpr e1
    emit " ? "
    inlineExpr e2
    emit " : "
    case e3 of
      Just e -> inlineExpr e
      Nothing -> emit "null"
    emit ")"

  PrimOpExpr _ op -> do
    emit "/* primop: "
    emit $ show op
    emit " */"

emitMatchPattern :: Pattern _ -> Emitter (List String)
emitMatchPattern = case _ of
  WildcardPattern _ -> do
    emit "$wild"
    pure Nil

  CtorPattern _ path args -> do
    emit "$ctor("
    intercalate (emit ".") (emit <$> path)
    bindings <- for args \a -> do
      emit ", "
      emitMatchPattern a
    emit ")"
    pure $ join bindings

  PatternVar _ name -> do
    emit "$bind"
    addLocal name
    pure (name : Nil)

  LiteralPattern _ lit -> do
    emit $ showLit lit
    pure Nil

emitMatchArm :: MatchArm _ -> Emitter Unit
emitMatchArm {pattern, definition} = scoped do
  emit "["
  bindings <- emitMatchPattern pattern
  emit ", ("
  intercalate (emit ", ") (emit <$> bindings)
  emit ") => "
  inlineExpr definition
  emit "]"

emitCallArg :: CallArg _ -> Emitter Unit
emitCallArg = case _ of
  ExprArg e -> inlineExpr e
  _ -> unsafeCrashWith "placeholder arg not implemented"

emitFunBody :: List (Stmt _) -> Maybe (Expr _) -> Emitter Unit
emitFunBody stmts final =
  do
    emitLine "{"
    indent
    traverse_ (\s -> emitIndent <> emitStmt s) stmts
    traverse_ emitReturn final
    deindent
    emitIndented "}"

emitReturn :: Expr _ -> Emitter Unit
emitReturn e = do
  emitIndented "return "
  inlineExpr e
  emitLine ";"

emitStmt :: Stmt _ -> Emitter Unit
emitStmt = case _ of
  DeclStmt _ Let name _ e -> do
    addLocal name
    emitConst name e

  DeclStmt _ Mut name _ e -> do
    addLocal name
    emitLet name e

  AssignGlobal _ path name e ->
    emitAssign (intercalate "." $ List.snoc path name) e

  AssignLocal _ i e ->
    getLocal i >>= \name -> emitAssign name e

  AssignRef _ _ _ ->
    unsafeCrashWith "assign ref"

  For _ pat e stmts -> scoped do
    emit "for (const "
    x <- scoped do
      emitPattern pat
      getLocal 0
    emit " of "
    addLocal x
    inlineExpr e
    emit ") "
    emitFunBody stmts Nothing
    emitLine ""

  While _ e stmts -> do
    emit "while ("
    inlineExpr e
    emit ") "
    emitFunBody stmts Nothing
    emitLine ""

  Loop _ stmts -> do
    emit "while (true) "
    emitFunBody stmts Nothing
    emitLine ""

  Break _ ->
    emitLine "break;"

  Continue _ ->
    emitLine "continue;"

  ExprStmt e -> do
    inlineExpr e
    emitLine ";"

emitAssign :: String -> Expr _ -> Emitter Unit
emitAssign name e = do
  emit name
  emit " = "
  inlineExpr e
  emitLine ";"

emitPattern :: Pattern _ -> Emitter Unit
emitPattern p = Tuple.fst $ runCounter (showPattern p)

showPattern :: Pattern _ -> Counter (Emitter Unit)
showPattern = case _ of
  WildcardPattern _ -> do
    i <- Counter.count
    Counter.incr
    pure (emit $ "_" <> show i)

  PatternVar _ x ->
    pure $ (addLocal x *> emit x)

  CtorPattern _ path args -> do
    mid <- intercalate (emit ", ") <$> sequence (showPattern <$> args)
    pure (emit (intercalate "." path) <> emit "(" <> mid <> emit ")")

  LiteralPattern _ lit ->
    pure $ emit $ showLit lit

showLit :: Literal -> String
showLit = case _ of
  IntLit i ->
    show i

  BoolLit true ->
    "true"

  BoolLit false ->
    "false"

  StringLit s ->
    "\"" <> s <> "\""

  UnitLit ->
    "undefined"

emitFunctionDef :: String -> List (DefParam _) -> Expr _ -> Emitter Unit
emitFunctionDef name params body = scoped do
  emit "function "
  emit name
  emit "("
  intercalate (emit ", ") (emitParam <$> params)
  emit ") "
  case body of
    BlockExpr _ stmts final ->
      emitFunBody stmts final <> emitLine ""
    _ -> do
      emitLine "{"
      indent
      emitReturn body
      deindent
      emitIndentedLine "}"

emitParam :: DefParam _ -> Emitter Unit
emitParam { name } = do
  addLocal name
  emit name

emitConst :: String -> Expr _ -> Emitter Unit
emitConst name e = do
  emit "const "
  emit name
  emit " = "
  inlineExpr e
  emitLine ";"

emitLet :: String -> Expr _ -> Emitter Unit
emitLet name e = do
  emit "let "
  emit name
  emit " = "
  inlineExpr e
  emitLine ";"

emitDecl :: Declaration _ -> Emitter Unit
emitDecl = case _ of
  Def _ _ name (Just params) _ body ->
    emitFunctionDef name params body

  Def _ _ name Nothing _ e ->
    emitConst name e

  Mod _ name _ -> do
    emit "// module "
    emitLine name

emitModule :: Module _ -> Emitter Unit
emitModule mod = do
  let { declarations } = mod
  intercalate (emit "\n\n") (emitDecl <$> declarations)

runEmitter :: Emitter Unit -> String
runEmitter em =
  runWriterT em
    # (\st -> runState st {indentLevel: 0, locals: Nil})
    # Tuple.fst
    # Tuple.snd

moduleToJS :: Module _ -> String
moduleToJS = runEmitter <<< emitModule

fileToJS :: Path -> Effect Unit
fileToJS path = launchAff_ do
  surfaceModule <- sequential $ parseFile path
  let boundModule = captureModuleVars <<< Tuple.snd <$> surfaceModule
  case boundModule of
    Error err -> do
      Console.log "parse error:"
      Console.log $ show err

    Ok mod -> do
      let {declarations} = mod
      Console.log $ intercalate "\n\n" $ printDeclaration <$> declarations
      Console.log "\n\n==========\n\n"
      let str = runEmitter $ emitModule mod
      Console.log(str)