
module Rook.Pass.AddFileInfo where

import Prelude

import Rook.Parse (SourcePos)
import Rook.Pass.Common (Location(..))
import Rook.Syntax.Path (Path)
import Text.Parsing.Parser.Pos (Position(..))

fromSourcePos :: Path -> SourcePos -> Location
fromSourcePos file { start : Position start, end : Position end } =
  Location { file, start, end }

addFileInfo :: forall ast. Functor ast => Path -> ast SourcePos -> ast Location
addFileInfo = fromSourcePos >>> map