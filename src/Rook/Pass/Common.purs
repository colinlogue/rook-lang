module Rook.Pass.Common where

import Prelude

import Data.Result (Result)
import Rook.Syntax.Path (Path)


type Pos = { line :: Int, column :: Int }

newtype Location = Location { file :: Path, start :: Pos, end :: Pos }

derive newtype instance Show Location

type ErrorReport =
  { location :: Location
  , message :: String
  }

newtype Simplification ast =
  Simplification (ast -> ast)

newtype Restructuring ast1 ast2 =
  Restructuring (ast1 -> ast2)

newtype Annotation (ast :: Type -> Type) meta1 meta2 =
  Annotation (ast meta1 -> ast meta2)

newtype Checking ast err =
  Checking (ast -> Result err ast)

type Pass (ast1 :: Type -> Type) info1 (ast2 :: Type -> Type) info2 err =
  { getError :: err -> ErrorReport
  , runPass :: ast1 info1 -> Result err (ast2 info2)
  }
