module Rook.Pass.Pass where

import Prelude

import Control.Monad.Error.Class (class MonadError, class MonadThrow)
import Control.Monad.State (class MonadState)
import Data.List (List)
import Data.Result (Result(..))
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Aff (Aff)
import Effect.Aff.Class (class MonadAff)
import Effect.Class (class MonadEffect, liftEffect)
import Rook.Syntax.Path (Path)


type Config =
  { files :: List Path
  }

type PassState = Unit

type PassError = String

newtype Pass a =
  Pass (Config -> PassState -> Aff (Tuple PassState (Result PassError a)))

instance Functor Pass where
  map :: forall a b. (a -> b) -> Pass a -> Pass b
  map f (Pass g) = Pass $ map (map (map (map (map f)))) g

instance Apply Pass where
  apply :: forall a b. Pass (a -> b) -> Pass a -> Pass b
  apply (Pass f) (Pass g) =
    Pass \c s ->
      do
        Tuple s' r1 <- f c s
        Tuple s'' r2 <- g c s'
        pure $ Tuple s'' $ r1 <*> r2

instance Applicative Pass where
  pure :: forall a. a -> Pass a
  pure x = Pass \_ s -> pure $ Tuple s $ Ok x

instance Bind Pass where
  bind :: forall a b. Pass a -> (a -> Pass b) -> Pass b
  bind (Pass f) g =
    Pass \c s ->
      do
        Tuple s' r1 <- f c s
        case g <$> r1 of
          Ok (Pass h) ->
            h c s'

          Error e ->
            pure $ Tuple s' $ Error e

instance Monad Pass

instance MonadState PassState Pass where
  state :: forall a. (PassState -> Tuple a PassState) -> Pass a
  state f =
    Pass \_ s ->
      do
        let Tuple x s' = f s
        pure $ Tuple s' $ Ok x

instance MonadThrow PassError Pass where
  throwError :: forall a. PassError -> Pass a
  throwError err = Pass \_ s -> pure $ Tuple s $ Error err

instance MonadError PassError Pass where
  catchError :: forall a. Pass a -> (PassError -> Pass a) -> Pass a
  catchError (Pass f) handle =
    Pass \c s ->
      do
        Tuple s' r <- f c s
        case r of
          Error err ->
            let Pass h = handle err
            in h c s'

          _ ->
            pure $ Tuple s' r

instance MonadEffect Pass where
  liftEffect :: forall a. Effect a -> Pass a
  liftEffect eff = Pass \_ _ -> pure <$> pure <$> liftEffect eff

instance MonadAff Pass where
  liftAff :: forall a. Aff a -> Pass a
  liftAff aff = Pass \_ _ -> pure <$> pure <$> aff