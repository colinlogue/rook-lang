module Rook.Pass.Full where

import Prelude

import Control.Bind (bindFlipped)
import Control.Monad.Comp (Comp(..), liftResult, runComp)
import Control.Monad.Error.Class (class MonadError, class MonadThrow, catchError, throwError)
import Control.Monad.State.Class (class MonadState)
import Control.Monad.State.Class as State
import Control.Parallel (sequential)
import Data.Array as Array
import Data.Bifunctor (lmap)
import Data.Either (Either(..))
import Data.Foldable (class Foldable, foldMap, foldl, intercalate, traverse_)
import Data.FoldableWithIndex (foldMapWithIndex, foldlWithIndex, traverseWithIndex_)
import Data.Generic.Rep (class Generic)
import Data.List (List(..), (:))
import Data.List as List
import Data.Map (Map)
import Data.Map as Map
import Data.Map.Extra as Map
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Newtype (class Newtype, unwrap)
import Data.Result (Result(..))
import Data.Result as Result
import Data.Set (Set)
import Data.Set as Set
import Data.Show.Generic (genericShow)
import Data.Traversable (sequence, traverse)
import Data.Tree (Tree(..))
import Data.Tree as Tree
import Data.Tuple (Tuple(..))
import Data.Tuple as Tuple
import Effect (Effect)
import Effect.Aff (Aff, ParAff, launchAff_, message, parallel)
import Effect.Aff.Class (class MonadAff, liftAff)
import Effect.Class (class MonadEffect, liftEffect)
import Effect.Class.Console (log)
import Node.Encoding (Encoding(..))
import Node.FS.Aff (readTextFile)
import Node.Path (FilePath)
import Node.Process (chdir)
import Partial.Unsafe (unsafeCrashWith)
import Rook.Annotated (meta)
import Rook.Binding (captureModuleVars)
import Rook.Context (Context)
import Rook.Context as Context
import Rook.Interp (Interp, InterpState, interp, interpExpr)
import Rook.Interp as Interp
import Rook.Parse (SourcePos)
import Rook.Parse as Parse
import Rook.Pass.AddFileInfo (fromSourcePos)
import Rook.Pass.Common (Location(..), ErrorReport)
import Rook.Syntax.Bound (Expr(..))
import Rook.Syntax.Bound as Bound
import Rook.Syntax.Common (DefParam, FunParam, Import, PrimOp(..), PrimType(..), RookType(..))
import Rook.Syntax.Path (Path)
import Rook.Syntax.Pattern (Pattern(..))
import Rook.Syntax.Surface as Surface
import Rook.Syntax.Value (Value)
import Rook.Syntax.Value as Value
import Rook.Types (TypeContext, TypeDef, TypeError, emptyTypeContext)
import Rook.Types as Types
import Text.Parsing.Parser (ParseError(..), runParser)
import Text.Parsing.Parser.Pos (Position(..))

type CompilationSetup =
  { files :: Array Path
  , root :: FilePath
  , main :: Path
  }

parseFile :: Path -> ParAff (Result ErrorReport (Surface.FileModule SourcePos))
parseFile path =
  parallel do
    let filePath = intercalate "/" path <> ".rk"
    contents <- readTextFile UTF8 filePath
    pure
      $ Result.fromEither
      $ lmap (\(ParseError msg (Position pos)) ->
          { location : Location { file : path, start : pos, end : pos }
          , message : msg
          })
      $ runParser contents
      $ Parse.file path

type Compilation a = Comp Aff Unit ErrorReport a

-- newtype Compilation a = Compilation (Comp Aff Unit ErrorReport a)

-- derive newtype instance Functor Compilation
-- derive newtype instance Apply Compilation
-- derive newtype instance Applicative Compilation
-- derive newtype instance Bind Compilation
-- derive newtype instance Monad Compilation
-- derive newtype instance MonadState Unit Compilation
-- derive newtype instance MonadThrow ErrorReport Compilation
-- derive newtype instance MonadError ErrorReport Compilation

-- instance MonadEffect Compilation where
--   liftEffect ex =
--     Compilation $ Comp \_ -> pure <$> pure <$> liftEffect ex

-- instance MonadAff Compilation where
--   liftAff ax =
--     Compilation $ Comp \_ -> pure <$> pure <$> ax

logErrors :: ErrorReport -> Compilation Unit
logErrors err =
  log $ "compilation error: " <> show err

data Definition = Definition String (RookType Unit) (Bound.Expr SourcePos)

derive instance Generic Definition _

instance Show Definition where
  show x = genericShow x

type SplitModule =
  { definitions :: List Definition
  , imports :: List (Import SourcePos)
  , path :: Path
  }

newtype Splitter a b = Splitter (Tuple (List a) (List b))

derive instance Newtype (Splitter a b) _
derive newtype instance Semigroup (Splitter a b)
derive newtype instance Monoid (Splitter a b)

right :: forall a b. b -> Splitter a b
right x = Splitter (Tuple Nil (pure x))

left :: forall a b. a -> Splitter a b
left x = Splitter (Tuple (pure x) Nil)

lefts :: forall a b f. Foldable f => f a -> Splitter a b
lefts xs = Splitter (Tuple (List.fromFoldable xs) Nil)

-- TODO: Should submodules inherit scope from parent modules?
-- TODO: Make sure this doesn't introduce module names that already exist.
-- Splits away any submodules contained in a module.
splitModule :: Path -> Bound.Module SourcePos -> List SplitModule
splitModule path { declarations, imports } =

  case foldMap splitDeclaration declarations # unwrap of

    Tuple mods definitions ->
      { definitions, imports, path } : mods

  where

    splitDeclaration :: Bound.Declaration SourcePos -> Splitter SplitModule Definition
    splitDeclaration = case _ of
      Bound.Def pos typeVars name params t e ->
        right $ Definition name (buildType typeVars params t) (buildExpr params t pos e)

      Bound.Mod _ name submod ->
        lefts $ splitModule (path <> pure name) submod

    buildExpr :: Maybe (List (DefParam SourcePos)) -> RookType SourcePos -> SourcePos -> Bound.Expr SourcePos -> Bound.Expr SourcePos
    buildExpr = case _ of
      Nothing ->
        \_ _ e -> e

      Just params ->
        \t pos -> Bound.FunExpr pos (toFunParam <$> params) (Just t)

    toFunParam :: DefParam SourcePos -> FunParam SourcePos
    toFunParam { name, annotation : t, info } =
      { pattern : PatternVar info name
      , annotation : Just t
      }


    buildType :: List String -> Maybe (List (DefParam SourcePos)) -> RookType _ -> RookType Unit
    buildType = case _ of
      Nil ->
        case _ of
          Just ps ->
            \t -> Arrow unit (ps <#> \p -> void p.annotation) $ void t

          Nothing ->
            \t -> void t

      xs ->
        \ps t -> Forall unit xs $ buildType Nil ps t

type TypeDefs = Map String (RookType Unit)

type Qualifications = Map String (Tuple Path (Set String))

type ModuleTypes =
  { typeDefs :: TypeDefs
  , qualifications :: Qualifications
  }

exports :: SplitModule -> TypeDefs
exports { definitions } =
  foldl
    (\acc (Definition name t _) -> Map.insert name t acc)
    Map.empty
    definitions

modTypes :: Map Path TypeDefs -> SplitModule -> Result ErrorReport ModuleTypes
modTypes env { imports, definitions, path } =
  typeDefs
    <#> { typeDefs : _, qualifications }
  where
    typeDefs :: Result ErrorReport TypeDefs
    typeDefs = foldl collect importTypeDefs definitions
      where
        collect :: Result ErrorReport TypeDefs -> Definition -> Result ErrorReport TypeDefs
        collect accResult (Definition name t e) =
          do
            acc <- accResult
            case Map.lookup name acc of
              Just _ ->
                Error
                  { location : fromSourcePos path $ meta e
                  , message : "multiple definitions for " <> name
                  }

              Nothing ->
                Ok $ Map.insert name t acc

    importTypeDefs :: Result ErrorReport TypeDefs
    importTypeDefs = foldl collect (Ok Map.empty) imports
      where
        collect :: Result ErrorReport TypeDefs -> Import SourcePos -> Result ErrorReport TypeDefs
        collect acc { qualified, path, names, info } =
          case qualified of
            Just _ ->
              acc

            Nothing ->
              case Map.lookup path env of
                Just modDefs ->
                  names
                    <#> (\x -> Map.lookup x.name modDefs
                        # Result.note
                            { location : fromSourcePos path info
                            , message :
                                "module "
                                <> show path
                                <> " does not export "
                                <> x.name
                            }
                        <#> Tuple x.name)
                    # sequence
                    <#> Map.fromFoldable

                Nothing ->
                  Error
                    { location : fromSourcePos path info
                    , message : "module " <> show path <> " is not defined"
                    }


    qualifications :: Qualifications
    qualifications = foldl collect Map.empty imports
      where
        collect :: Qualifications -> Import _ -> Qualifications
        collect acc { qualified, path, names } =
          case qualified of
            Just alias ->
              let
                namesSet = Set.fromFoldable (names <#> \x -> x.name)
              in
                Map.insert alias (Tuple path namesSet) acc

            Nothing ->
              acc

fileLoc :: Path -> Location
fileLoc file = Location { file, start : origin, end : origin }
  where
    origin = { column : 0, line : 0 }

combineSplitModules :: Compilation (Map Path SplitModule) -> SplitModule -> Compilation (Map Path SplitModule)
combineSplitModules compAcc mod =
  compAcc >>= \acc ->
    Map.insertUnique mod.path mod acc
      # Result.note
          { location : fileLoc mod.path
          , message : "module appears twice: " <> show mod.path
          }
      # liftResult

checkForCircularImports :: Map Path SplitModule -> Compilation Unit
checkForCircularImports mods = traverse_ (\p -> go p Set.empty p) $ Map.keys mods
  where
    go :: Path -> Set Path -> Path -> Compilation Unit
    go checking seen current =
      if
        Set.member current seen
      then
        throwError
          { location : fileLoc checking
          , message : "circular import: " <> show current
          }
      else
        case Map.lookup current mods of
          Just { imports } ->
            traverse_ (\{ path } -> go checking (Set.insert path seen) path) imports

          Nothing ->
            throwError
              { location : fileLoc current
              , message : "no module at path " <> show current
              }

buildTree :: Map Path SplitModule -> Path -> Tree SplitModule
buildTree mods path =
  case Map.lookup path mods of
    Just mod@{ imports } ->
      Node mod (imports <#> \{ path : p } -> buildTree mods p)

    Nothing ->
      unsafeCrashWith "crashed in buildTree"

checkDef :: TypeContext -> Path -> Definition -> Compilation Unit
checkDef cxt file (Definition _ t e) =
  liftResult
    $ lmap (fromTypeError file $ meta e)
    $ Types.runTC cxt (Types.checkExpr t (e <#> \{ start } -> start))

fromTypeError :: Path -> SourcePos -> TypeError -> ErrorReport
fromTypeError file pos err =
  { location : fromSourcePos file pos
  , message : show err
  }

checkModule :: TypeContext -> Path -> SplitModule -> Compilation Unit
checkModule cxt path mod = traverse_ (checkDef cxt path) mod.definitions

typeCheck :: Map Path SplitModule -> Path -> Compilation Unit
typeCheck mods path =
  do
    mod <- Map.lookup path mods
      # Result.note
        { location : fileLoc path
        , message : "Unknown module: " <> show path
        }
      # liftResult
    traverse_ (checkDef cxt path) mod.definitions
  where
    env :: Map Path (Map String TypeDef)
    env = mods
      <#> exports
      <#> map
        { isMutable : false
        , type : _
        , position : Position { column : 0, line : 0 }
        }

    cxt :: TypeContext
    cxt = emptyTypeContext { context = Context.empty { globals = env } }


--     Nothing ->
--       throwError
--         { location : fileLoc path
--         , message : "module " <> show path <> " not found"
--         }

-- typeContext :: ModuleTypes -> TypeContext
-- typeContext { qualifications, typeDefs } = emptyTypeContext { context = context }
--   where
--     context :: Context TypeDef
--     context = ?cxt

typeContext :: Map Path SplitModule -> TypeContext
typeContext mods = cxt
  where
    env :: Map Path (Map String TypeDef)
    env = mods
      <#> exports
      <#> map
        { isMutable : false
        , type : _
        , position : Position { column : 0, line : 0 }
        }

    cxt :: TypeContext
    cxt = emptyTypeContext { context = Context.empty { globals = env } }

origin :: SourcePos
origin = { start : Position { column : 0, line : 0 }, end : Position { column : 0, line : 0 } }

base :: SplitModule
base =
  { path : "Base" : Nil
  , definitions : List.fromFoldable
    [ Definition
        "print"
        (Arrow unit (Primitive unit StringType : Nil) (Primitive unit UnitType))
        (PrimOpExpr origin Print)
    ]
  , imports : Nil
  }

-- -- TODO: mutually recursive definitions / out-of-order defs
-- interpModule :: SplitModule -> Interp Unit
-- interpModule { definitions, imports } =
--   do

type Env a = Map Path (Map String a)

-- create a module-specific environment from the global env
getEnv :: forall a. Env a -> SplitModule -> Compilation (Env a)
getEnv env { imports, path : file } = foldl collect (pure Map.empty) imports
  where
    collect :: Compilation (Env a) -> Import _ -> Compilation (Env a)
    collect acc { path, qualified, names, info } =
      do
        defs <- Map.lookup path env
          # Result.note
            { location : fromSourcePos file info
            , message : "module not found: " <> show path
            }
          # liftResult
        let path' = fromMaybe path $ pure <$> qualified
        defs' <- foldl (keepNames path defs) (pure Map.empty) names
        acc >>= addDefs path' defs'

    keepNames :: Path -> Map String a -> Compilation (Map String a) -> { name :: String, info :: SourcePos } -> Compilation (Map String a)
    keepNames path defs acc { name, info } =
      case Map.lookup name defs of
        Just val ->
          acc <#> Map.insert name val

        Nothing ->
          throwError
            { location : fromSourcePos file info
            , message : "can't import " <> name <> " from " <> show path
            }

    addDefs :: Path -> Map String a -> Env a -> Compilation (Env a)
    addDefs path defs acc =
      case Map.lookup path acc of
        Nothing -> pure $ Map.insert path defs acc
        Just prevDefs ->
          do
            combinedDefs <- foldlWithIndex mixDefs (pure prevDefs) defs
            pure $ Map.insert path combinedDefs acc

    mixDefs :: String -> Compilation (Map String a) -> a -> Compilation (Map String a)
    mixDefs name cAcc val =
      cAcc >>= \acc -> case Map.lookup name acc of
        Nothing -> pure $ Map.insert name val acc
        Just _ -> throwError
          { location : fileLoc file
          , message : "multiple definitions for " <> name
          }

interpModule :: Env Value -> SplitModule -> Compilation (Map String Value)
interpModule env mod =
  do
    globals <- getEnv env mod
    let init = Interp.initialState globals
    defs <- traverse interpDef mod.definitions
      # runComp init
      # liftEffect
      <#> lmap (\message -> { location : fileLoc mod.path, message })
      <#> map Tuple.snd
      >>= liftResult
    pure $ Map.fromFoldable defs

interpDef :: Definition -> Interp (Tuple String Value)
interpDef (Definition name _ e) =
  do
    val <- interpExpr $ void e
    State.modify_ \s ->
        s { globals = Map.update (pure <<< Map.insert name val) Nil s.globals }
    pure $ Tuple name val

eval :: Compilation (Env Value) -> SplitModule -> Compilation (Env Value)
eval acc mod =
  do
    env <- acc
    vals <- interpModule env mod
    pure $ Map.insert mod.path vals env

compile :: CompilationSetup -> Compilation Unit
compile { root, files, main } =
  do
    log $ "compiling " <> show (Array.length files ) <> " files"
    liftEffect $ chdir root
    fileContents <- liftAff $ sequential $ traverse parseFile files
    parsedModules <- liftResult $ Map.fromFoldable <$> sequence fileContents
    let boundModules = captureModuleVars <$> parsedModules
    let splitModuleList = base : foldMapWithIndex splitModule boundModules
    splitModules <- foldl combineSplitModules (pure Map.empty) splitModuleList
    -- TODO: This isn't working
    -- checkForCircularImports splitModules
    let cxt = typeContext splitModules
    log "typechecking modules"
    traverseWithIndex_ (checkModule cxt) splitModules
    let evalOrder = Tree.postOrder $ buildTree splitModules main
    env <- foldl eval (pure Map.empty) evalOrder
    log "running main function"
    mainFunc <- Map.lookup main env
      >>= (\mainModule -> Map.lookup "main" mainModule)
      # Result.note
        { location : fileLoc main
        , message : "main function not defined"
        }
      # liftResult
    let interpState = Interp.initialState env
    val <- liftEffect $ interp interpState $ Bound.Call (Value.toExpr mainFunc) unit Nil
    log $ "finished with value " <> show val
  `catchError` logErrors

runCompilation :: CompilationSetup -> Effect Unit
runCompilation setup =
  launchAff_ do
    let c = compile setup
    runComp unit c
