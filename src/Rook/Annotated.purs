module Rook.Annotated where

-- import Rook.Syntax

class Annotated a where
  meta :: forall info. a info -> info

-- instance annotatedExpr :: Annotated Expr where
--   meta e =
--     case e of
--       Global x _ _ ->
--         x

--       Local x _ ->
--         x

--       Ctor x _ _ ->
--         x

--       Block x _ _ ->
--         x

--       If x _ _ _ ->
--         x

--       FunExpr x _ _ _ ->
--         x

--       Match x _ _ ->
--         x

--       Lit x _ ->
--         x

--       Call x _ _ ->
--         x

-- instance annotatedStmt :: Annotated Stmt where
--   ann s =
--     case s of
--       DeclStmt x _ _ _ _ ->
--         x

--       AssignGlobal x _ _ _ ->
--         x

--       AssignLocal x _ _ ->
--         x

--       For x _ _ _ ->
--         x

--       While x _ _ ->
--         x

--       Loop x _ ->
--         x

--       ControlStmt x _ ->
--         x

--       ExprStmt e  ->
--         ann e
