module Rook.Context where

import Prelude

import Data.Foldable (class Foldable, foldM)
import Data.Map (Map)
import Data.Map as Map
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Result (Result(..))
import Data.Set (Set)
import Data.Set as Set
import Data.Stack (Stack)
import Data.Stack as Stack
import Data.Tuple (Tuple(..))
import Partial.Unsafe (unsafeCrashWith)
import Rook.Syntax.Path (Path)


type Context a =
  { globals :: Map Path (Map String a)
  , locals :: Stack { name :: String, val :: a }
  , localNames :: Set String
  }

data GlobalError
  = UnknownModule
  | NameAlreadyDefined
  | UnknownName
  | LocalOutOfRange

data LocalError a
  = Shadowing a

addGlobal :: forall a. Path -> String -> a -> Context a -> Result GlobalError (Context a)
addGlobal path name val cxt =
  case Map.lookup path cxt.globals of
    Just mod ->
      if
        Map.member name mod
      then
        Error NameAlreadyDefined
      else
        Ok cxt { globals = Map.insert path (Map.insert name val mod) cxt.globals }

    Nothing ->
      Error UnknownModule

updateGlobal :: forall a. Path -> String -> a -> Context a -> Result GlobalError (Context a)
updateGlobal path name val cxt =
  case Map.lookup path cxt.globals of
    Just mod ->
      if
        Map.member name mod
      then
        Ok cxt { globals = Map.insert path (Map.insert name val mod) cxt.globals }
      else
        Error UnknownName

    Nothing ->
      Error UnknownModule

updateLocal :: forall a. Int -> a -> Context a -> Result GlobalError (Context a)
updateLocal i val cxt@{ locals } =
    case Stack.popMany i locals of
      Just { earlier, current, later } ->
        Ok cxt
          { locals =
              earlier
                # Stack.push current { val = val }
                # Stack.pushMany (Stack.asList later)
          }

      Nothing ->
        Error LocalOutOfRange

empty :: forall a. Context a
empty = { globals : Map.empty, locals : Stack.empty, localNames : Set.empty }

extend :: forall a. String -> a -> Context a -> Result (LocalError a) (Context a)
extend name val cxt@{ locals, localNames } =
  if
    Set.member name localNames
  then
    Error $ findShadow locals
  else
    Ok cxt
      { locals = Stack.push x locals
      , localNames = Set.insert name localNames
      }
  where
    x = { name, val }

    findShadow stack =
      case Stack.pop stack of
        Just (Tuple stack' { name : name', val }) ->
          if name == name' then Shadowing val else findShadow stack'

        Nothing ->
          unsafeCrashWith
            $ "invariant not satisfied in context: "
              <> "name " <> name <> " appears in localNames but not in locals"

retract :: forall a. Context a -> Maybe (Context a)
retract cxt@{ locals, localNames } = map go $ Stack.pop locals
  where
    go (Tuple locals' { name }) =
      cxt { locals = locals', localNames = Set.delete name localNames }

drop :: forall a. Int -> Context a -> Maybe (Context a)
drop = \n cxt ->
  if
    n < 0
  then
    Nothing
  else
    go n (Just cxt)
  where
    go :: Int -> Maybe (Context a) -> Maybe (Context a)
    go n cxt =
      if
        n == 0
      then
        cxt
      else
        go (n - 1) (cxt >>= retract)

add :: forall f a. Foldable f => f { name :: String, val :: a } -> Context a -> Result (LocalError a) (Context a)
add vals cxt = foldM (\acc {name, val} -> extend name val acc) cxt vals

class Lookup a where
  lookup :: forall b. a -> Context b -> Maybe b

instance Lookup (Tuple Path String) where
  lookup :: forall a. Tuple Path String -> Context a -> Maybe a
  lookup (Tuple path name) { globals } =
    Map.lookup path globals >>= Map.lookup name

instance Lookup Int where
  lookup :: forall a. Int -> Context a -> Maybe a
  lookup i { locals } =
    if
      i < 0
    then
      Nothing
    else
      go i locals
    where
      go :: forall b r. Int -> Stack { val :: b | r} -> Maybe b
      go n stack =
        do
          Tuple rest x <- Stack.pop stack
          if n == 0 then pure x.val else go (n - 1) rest

