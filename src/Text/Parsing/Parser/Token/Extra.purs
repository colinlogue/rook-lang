module Text.Parsing.Parser.Token.Extra (lower) where

import Prelude

import Data.CodePoint.Unicode (isLower)

import Data.String (CodePoint)
import Data.String.CodePoints (codePointFromChar)
import Text.Parsing.Parser (ParserT)
import Text.Parsing.Parser.Combinators ((<?>))
import Text.Parsing.Parser.String (satisfy)

satisfyCP :: forall m . Monad m => (CodePoint -> Boolean) -> ParserT String m Char
satisfyCP p = satisfy (p <<< codePointFromChar)

lower :: forall m . Monad m => ParserT String m Char
lower = satisfyCP isLower <?> "uppercase letter"
