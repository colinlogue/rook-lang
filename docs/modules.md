# Modules

## File modules
A file module declaration has the form `module <Path>:` and
denotes that the remainder of the file is the module
definition.

## Submodules
A module can be declared within a file or within another
module using the syntax `mod <Name> { <definitions> }`.

```
module Some.Module:

import Some.Other.Module as ImpMod { a, b }

export { x, SomeMod }

-- This one is in the export list, so it is public.
def x : SomeType := someEpr

-- This is not in the export list, so it is private.
def y : AnotherType := anotherExpr

mod Submod {
  export { z }
  def z : SomeOtherType := someOtherExpr 
}
```

## Declarations
A module is a series of declarations.

## Running a module
To run a program from some source files:

1. Parse each source file into a `Rook.Syntax.Module`.
2. Create a dependency graph and check that each module
path is unique, there are no cycles, and each required
module is defined.
3. Create a `Map ModulePath (Map String (RookType Unit))`
to store the types of each declared item.
3. Typecheck each source file:
   1. If any of its imports have not been typechecked yet,
   check them first.
   2. 