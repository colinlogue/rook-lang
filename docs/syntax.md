## Syntax

### Placeholders in functions calls
If we have a function `f` of type `(Int, Bool) -> Int`, we
can partially apply it by calling it with a placeholder to
produce either a function of type `(Int) -> Int` or of type
`(Bool) -> Int`. The placeholder is notated with `%_`, so
the first case would be `f(%_, true)` while the second
would be `f(1, %_)`. So the following are all equivalent:
```
f(1, true)
f(%_, true)(1)
f(1, %_)(true)
f(%_, %_)(1, true)
(x => f(x, true))(1)
(x => f(1, x))(true)
((x,y) => f(x,y))(1, true)
(x => y => f(x,y))(1)(true)
```


```
foldl<a,b>(f: (a,b) -> b, init: b, list: List<a>) -> b =
  match list {
    Nil => init,
    Cons(x,xs) => foldl(f, f(x, init), xs)
  };

all<a>(pred: a -> Bool, list: List<a>) -> Bool =
  foldl(and(pred(%_), %_), True, list);

all<a>(pred: a -> Bool, list: List<a>) -> Bool =
  foldl((x,b) => and(pred(x),b)), True, list);
```

```
total(list: List<Int>) -> Int {
  mut count <- 0;
  list.forEach({ x <- x + %_ });
  count
}
```

### Types

```
map [a,b] (list: List<a>, fn: a -> b) -> List<b>
```